#!/usr/bin/python
import os
import datetime
import sqlite3

days = ["201103%02d" % (day,) for day in range(9, 32)] + ["20110401", "20110402",]

print days

out_file = open("%s/sendai-experiments/results/hashtag_counts.csv" % (os.environ['HOME'],), "w")
out_file.write("day,with hashtags, total, percentage\n")

for day in days:
    conn = sqlite3.connect("%s/sendai-segmented/%s.db" % (os.environ['SCRATCH'], day,))
    cursor = conn.cursor()
    for row in cursor.execute("""SELECT COUNT(*) FROM tweets WHERE body LIKE '%%#%%'"""):
        hashtag_count = row[0]
    for row in cursor.execute("""SELECT COUNT(*) FROM tweets"""):
        total_tweets = row[0]
    print "%s,%s,%s,%s\n" % (day, hashtag_count, total_tweets, float(hashtag_count) / total_tweets,)
    out_file.write("%s,%s,%s,%s\n" % (day, hashtag_count, total_tweets, float(hashtag_count) / total_tweets,))

out_file.close()
