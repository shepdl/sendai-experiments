#!/usr/bin/python

import project
import utility

import project
import utility

def percentage_of_retweets_per_hour(filename):
    total_tweets = {}
    retweet_percentages = { 
        "day" : date_from(filename),
    }
    for row in cursor.execute(""" SELECT strftime("%Y-%m-%d %H:00:00", tweeted_at) AS hour, 
            COUNT(*) AS total FROM tweets
            GROUP BY strftime("%Y-%m-%d %H:00:00", tweeted_at)
        """):
        total_tweets[row[0]] = float(row[1])
    for row in cursor.execute(""" SELECT strftime("%Y-%m-%d %H:00:00", tweeted_at) AS hour, 
            COUNT(*) AS total FROM tweets
            WHERE body LIKE "%RT%"
            GROUP BY strftime("%Y-%m-%d %H:00:00", tweeted_at)
        """):
        retweet_percentages[row[0]] = row[1] / total_tweets[row[0]]
    return retweet_percentages

results = distribute(project.DATASET, percentage_of_retweets_per_hour)
if is_supervisor():
    results = flatten(results)
    
    # write to CSV
    column_headers = ["day",] + ["%02d:00:00" % (x,) for x in range(24)]
    out_file = project.open_result_file("percentage-of-retweets-per-hour.csv")
    out_file.writeheader()
    out_file.writerows(hourly_counts)
    out_file.close()