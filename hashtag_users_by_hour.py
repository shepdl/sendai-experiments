# hashtag_networks -- discover how hashtags are connected over
# time.
#
# find all hashtags in all tweets. make a dictionary of their
# connections. after each hour, write a summary to disk and flush.
# express this as proportion of total tweets, or total connections?
# then, compute the average deviation in connection over time,
# perhaps? or average deviation in connection between this hour and
# the previous hour?

sample_hashtag_connection = {
    "#tahrir" : {
        "#egypt" : 3,
    },
    "#egypt" : {
        "#tahrir" : 3, # note: this is always reciprocal
    },
}

sample_return = {
    "{hour}" : {
        "tags" : {
            "tag1": {
                "user1" : 5,
                "user2" : 1,
            },
        }, "users" : {
            "user1" : {
                "tag1" : 5,
            },
        },
    },
}
def hashtag_users_by_hour(cursor):
    """
        Discover how the hashtags that users use change over time.

        The purpose of this metric is to see if there are patterns in users' affinities for hashtags. This is kind of abstract for the moment, but we want to do this to see if it raises interesting questions.
        Also, it answers some interesting questions: how much do users "go on with their lives" during disasters? How much time do they spend tweeting about other things? This also provides some metrics to demonstrate users' sense of an event: how much will they tweet about a larger event?  

        Output: Hourly CSV files of matrix of users by hour. 
    """
    data = {}
    for row in cursor.execute("""
            SELECT strftime("%Y-%m-%d %H:00:00", tweeted_at) AS hour
                body, nickname FROM tweets
            """):
        hour = data.setdefault("hour", {
            "tags": {}, "users" : {},
        })
        # TODO: use NetworkX
        user = row["username"]
        if len(tags_from(row["body"])) == 0:
            hour["tags"][project.NULL_TAG] += 1
        else:
            for tag in tags_from(row["body"]):
                hour["tags"][tag].setdefault(user, 0)
                hour["tags"][tag][user] += 1
                hour["users"][user].setdefault(tag, 0)
                hour["users"][user][tag] += 1
        # TODO: now compare each hour to previous hour
        # do this by separating these out to other processes
    return data

def main():
    # distribute
    # run
