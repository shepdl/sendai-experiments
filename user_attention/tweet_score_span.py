import os
import json
import codecs

bursts = json.load(codecs.open("%s/burst_list_0.json" % (os.environ['SCRATCH'],), encoding="utf-8-sig"))
burst_offsets = bursts.keys()
user_data = json.load(codecs.open("%s/sendai-output/user_attention/full_data.json" % (os.environ['SCRATCH'],), encoding="utf-8-sig"))

user_scores = {}
user_scores_during_bursts = {}
tweet_scores = []
for user in user_data: 
    user_scores[user['username']] = {
        'total' : sum(user['relevance_counts']),
        'average' : sum(user['relevance_counts']) / len(user['relevance_counts'])
    }
    score_during_bursts = 0
    for tweet in user['word_scores']:
        offset, words = tweet
        if offset in burst_offsets:
            scores = []
            if len(words) == 0:
                tweet_scores.append(0)
            else:
                this_tweet_score = sum([
                    score for _, score in words
                ])
                tweet_scores.append(this_tweet_score)
                score_during_bursts += this_tweet_score
    user_scores[user['username']]['during_bursts'] = score_during_bursts


out_file = codecs.open("user_scores.csv", "w", encoding="utf-8-sig")
out_file.write("username,total,average,during bursts\n")
for username, data in user_scores.iteritems():
    out_file.write("%s,%s,%s,%s\n" % (username, data['total'], data['average'], data['during_bursts']))
out_file.close()

out_file = codecs.open("tweet_scores.csv", "w", encoding="utf-8-sig")
out_file.write("\n".join([str(s) for s in user_scores]))
out_file.close()


