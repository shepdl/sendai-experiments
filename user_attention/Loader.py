"""
Loader: Loads tweets by category
"""

import os
import codecs
import json

import sqlite3

from config import AGGREGATOR_NODE_ID
from node import Node

from utility import *

import numpy as np
import scipy.cluster.vq
from mpi4py import MPI

from user_sample import USERS
# from burst_list import BURSTS
# from word_burst_distribution import SCORE_DISTRIBUTION

COMM = MPI.COMM_WORLD

SCORE_DISTRIBUTION = json.load(codecs.open("%s/word_burst_distribution_%s.json" % (
    os.environ['SCRATCH'], COMM.Get_rank()
), encoding="utf-8-sig"))

def date_to_min_offset(date_string):
    _, month, day = date_string.split(" ")[0].split("-")
    time_component = date_string.split(" ")[1].split(":")
    hour = time_component[0]
    minutes = int(time_component[1])
    if minutes >= 30:
        minutes = 30
    else: 
        minutes = 0
    offset = (
        (int(day) - 9 if month == "03" else 
            int(day) + 22) * 1440 +
        (int(hour) * 60) +
        minutes
    )
    return offset

def offset_in_range(offset, range):
    if int(offset) >= int(range[0]) and int(offset) >= int(range[1]):
        return True
    else: 
        return False

class Loader(Node):

    def __init__(self):
        super(Loader, self).__init__("Loader")
        # figure out which db file to load
        self.filename = (["201103%02d" % (x + 9,) for x in range(23)] + 
            ["20110401", "20110402",])[COMM.Get_rank()]
        self.bursts = {}
        self.clusters = 6
        bursts = json.load(codecs.open("%s/burst_list_%s.json" % (
            os.environ['SCRATCH'], COMM.Get_rank()), encoding="utf-8-sig"))
        for burst in bursts:
            span = tuple([int(x) for x in burst.split(" to ")])
            self.bursts[span] = bursts[burst]
        # for offset, words in BURSTS.iteritems():
        #     self.bursts[int(offset)] = words.strip().split("/")
        self.setup_dispatch_table({
            'load_for_users' : self.load_for_users,
            'process_users' : self.process_users,
            'do_1_dim_kmeans' : self.do_1_dim_kmeans,
            'do_2_dim_kmeans' : self.do_2_dim_kmeans,
        })

    def load_for_users(self):
        self.log("Searching for tweets by users ...")
        conn = sqlite3.connect("%s/sendai-segmented/%s.db" % (
            os.environ['SCRATCH'], self.filename
        ))
        cursor = conn.cursor()
        tweets = dict([(user, [],) for user in USERS])
        for row in cursor.execute(""" SELECT username, date, segmented_body
                FROM tweets
                """):
            user, timestamp, body = row
            if user in tweets:
                tweets[user].append((date_to_min_offset(timestamp), body,))
        COMM.send({
            "name" : "loaded_tweets",
            "data" : {
                "tweets" : tweets,
                "node_id" : COMM.Get_rank(),
            }
        }, dest=AGGREGATOR_NODE_ID)


    def process_users(self, user, tweets):
        """
            Write to local storage or scratch file with username: 
                { 'username' : username, 'relevance_counts' : [], 'relevance_score' : 0.1234 }
        """
        self.log("Analyzing tweets by %s" % (user,))
        # relevance_score = 0.0
        relevance_scores = []
        dbg_hit_relevant_word = False
        words = []
        for offset, tweet in tweets:
            score = 0.0
            this_tweet = []
            tweet = tweet.split()
            # offsets in burst_list are stored as strings
            offset = int(offset)
            # if offset in self.bursts or str(offset) in self.bursts or int(offset) in self.bursts:
            for range in self.bursts:
                if offset_in_range(offset, range):
                    # offset = str(offset)
                    for word in tweet:
                        word = word.lower()
                        if word in self.bursts[range]:
                            # if not dbg_hit_relevant_word:
                            #     print "Found relevant word: %s" % (word,)
                            #     dbg_hit_relevant_word = True
                            # Add 1 to compensate for there being a zeroth
                            # breakdown
                            this_word_score = 1.0 # eliminating commonality because words are rare now / (int(SCORE_DISTRIBUTION[word]) + 1)
                            score += this_word_score
                            this_tweet.append((word, this_word_score,))
                    break
            words.append((offset, this_tweet,))
            relevance_scores.append(score)
        out_file = codecs.open(
            "%s/sendai-output/user_attention/individual_user_scores/%s.json" % (
            os.environ['SCRATCH'], user,
        ), "w", encoding="utf-8-sig")
        json.dump({ 
            'username' : user, 
            'word_scores' : words,
            'relevance_counts' : relevance_scores, 
            'score' : score,
        }, out_file, indent=4)
        out_file.close()
        COMM.send({
            "name" : "processing_users_done",
            "data" : {
                "node_id" : COMM.Get_rank(),
            },
        }, dest=AGGREGATOR_NODE_ID)

    def send_aggregates(self):
        pass

    def do_1_dim_kmeans(self, dataset):
        self.log("Doing kmeans on 1 dimension")
        in_data = []
        for user in dataset:
            relevance_score = user['score']
            relevance_counts = len(user['relevance_counts'])
            in_data.append(relevance_score / relevance_counts)
        in_data = np.array(in_data)
        centroids, labels = scipy.cluster.vq.kmeans2(in_data, self.clusters)
        user_clusters = dict([(cluster_id, [],) 
            for cluster_id in range(self.clusters)]
        )
        for index in range(self.clusters):
            user_clusters[labels[index]].append(dataset[index])
        out_file = codecs.open(
            "%s/sendai-output/user_attention/1_dim_clusters.txt" % (
            os.environ['SCRATCH'],), "w", encoding="utf-8-sig"
        )
        user_cluster_keys = user_clusters.keys()
        for index in range(len(user_clusters)):
            centroid = centroids[index]
            key = user_cluster_keys[index]
            users = user_clusters[key]
            out_file.write("""Cluster %s (centroid: %s) \n""" % (
                    index, centroid,
                )
            )
            out_file.write("Username\tRelevance Score\tTweets")
            for user in users:
                out_file.write("""%s\t%s\t%s""" % (
                    user['username'], user['relevance_score'], 
                    user['relevance_counts'],
                ))
        out_file.close()
        return self.STOP_LISTENING

    def do_2_dim_kmeans(self, dataset):
        self.log("Doing kmeans on 2 dimensions")
        in_data = []
        for user in dataset:
            relevance_score = user['score']
            relevance_counts = len(user['relevance_counts'])
            in_data.append( (relevance_counts, 
                relevance_score / relevance_counts,) )
        in_data = np.array(in_data)
        centroids, labels = scipy.cluster.vq.kmeans2(in_data, self.clusters)
        user_clusters = dict([(cluster_id, [],) 
            for cluster_id in range(self.clusters)])
        for index in range(self.clusters):
            user_clusters[labels[index]].append(dataset[index])
        out_file = codecs.open(
            "%s/sendai-output/user_attention/2_dim_clusters.txt" % (
            os.environ['SCRATCH'],), "w", encoding="utf-8-sig"
        )
        user_cluster_keys = user_clusters.keys()
        for index in range(len(user_clusters)):
            centroid = centroids[index]
            key = user_cluster_keys[index]
            users = user_clusters[key]
            out_file.write("""Cluster %s (centroid: %s) \n""" % (
                    index, centroid,
                )
            )
            out_file.write("Username\tRelevance Score\tTweets")
            for user in users:
                out_file.write("""%s\t%s\t%s""" % (
                    user['username'], user['relevance_score'], 
                    user['relevance_counts'],
                ))
        out_file.close()
        return self.STOP_LISTENING
