import itertools

from mpi4py import MPI
COMM = MPI.COMM_WORLD

def raise_fatal_exception(ex):
    print ex
    MPI.send(ex, DISPATCHER)

def message_queue():
    while True:
        message = COMM.recv(source=MPI.ANY_SOURCE)
        if message["name"] == "stop":
            break
        yield message


def for_rank(start=0, through=None, ids=None):
    if through is not None:
        range_to_test = map(lambda i: start + i, range(i))
    elif ids is not None:
        range_to_test = ids
    else:
        range_to_test = [start,]
    if COMM.Get_rank() in range_to_test:
        return Role()
    else: 
        return WrongRole()

class Role(object):
    def assign_role_of(self, klass):
        self.klass = klass
        return self

    def start(self ):
        self.klass = self.klass()
        self.klass.run()

class WrongRole(Role):
    def assign_role_of(self, klass):
        return self

    def start(self):
        pass

def run(items):
    this_node = filter(lambda i: i.__class__ != WrongRole, itertools.chain(items))[0]
    this_node.start()
    return this_node

