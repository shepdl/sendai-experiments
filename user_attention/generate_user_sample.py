"""
    Sample users: get 5000 users to sample for user tweet counting.
"""

import codecs
import sqlite3

conn = sqlite3.connect("../results/total-tweets-per-user.db")
cursor = conn.cursor()

# only include users who sent more than 10 tweets
users = []
for row in conn.execute(""" SELECT username FROM results WHERE total > 10 """):
    users.append(row[0])

# get every 100th user

out_file = codecs.open("user_sample.py", "w", encoding="utf-8-sig")
out_file.write("USERS = [\n")
for user in users[::100]:
    out_file.write(""" "%s", \n""" % (user,))

out_file.write("]\n")
