"""
    AggregatorController: Handles aggregation and control
"""

import os
import glob
import codecs
import json

from mpi4py import MPI
COMM = MPI.COMM_WORLD

from user_sample import USERS
from node import Node

from config import LOADER_NODE_IDS

class AggregatorController(Node):

    def __init__(self):
        super(AggregatorController, self).__init__("AggregatorController")
        self.available_loaders = dict([
            (node_id, False) for node_id in LOADER_NODE_IDS
        ])
        self.user_tweets = dict([
            (user, []) for user in USERS
        ])
        self.aggregates = []
        self.setup_dispatch_table({
            'loaded_tweets' : self.loaded_tweets,
            'processing_users_done' : self.processing_users_done,
            'merge_aggregates' : self.merge_aggregates,
        })

    # NOTE: Don't think I need a run() method because the Loaders
    # activate themselves and get the nodes to track from the file
    def run(self):
        for loader in LOADER_NODE_IDS:
            COMM.send({
                "name" : "load_for_users",
                "data" : {},
            }, dest=loader)
        super(AggregatorController, self).run()

    def get_available_loaders(self):
        """
            Get list of available loader nodes.
        """
        return [loader for loader, status in 
            self.available_loaders.iteritems() if status
        ]

    def all_loaders_free(self):
        """
            Get list of available loaders.
        """
        return len(self.get_available_loaders()) == len(LOADER_NODE_IDS)

    def send_tweets(self):
        available_loaders = self.get_available_loaders()
        for loader in available_loaders:
            for user in self.user_tweets.keys()[0:len(available_loaders)]:
                COMM.send({
                    "name": "process_users",
                    "data" : {
                        "user": user,
                        "tweets" : self.user_tweets[user],
                    }
                }, dest=loader)
                del self.user_tweets[user]

    def loaded_tweets(self, tweets, node_id):
        self.log("Received tweets from %s" % (node_id,))
        # store tweets
        for username, tweet_list in tweets.iteritems():
            self.user_tweets[username].extend(tweet_list)
        self.available_loaders[node_id] = True
        # if there are no loading jobs outstanding, send tweets 
        # back to loaders to process
        if self.all_loaders_free():
            self.send_tweets()
                

    def processing_users_done(self, node_id):
        """
            Finished all processing for one user
        """
        # 2 paths:
        # if we still have users to load, call send_users
        self.available_loaders[node_id] = True
        self.log("Node %s finished processing" % (node_id,))
        if len(self.user_tweets) > 0:
            self.send_tweets()
        # if we're finished loading users, call 'process_users'
        else:
            if self.all_loaders_free():
                self.merge_aggregates()
                # COMM.send({
                #     "message" : "send_aggregates",
                #     "data" : {},
                # })
            else:
                self.log("Users exhausted; waiting for remaining nodes.")

    def merge_aggregates(self): #, node_id, aggregates):
        """
            Write down total aggregate files and activate last files.
        """
        self.log("Merging aggregates ...")
        # self.available_loaders[node_id] = True
        # self.aggregates.append(aggregates)
        self.aggregates = []
        for in_file in glob.glob(
            "%s/sendai-output/user_attention/individual_user_scores/*.json" % os.environ['SCRATCH']
            ):
            self.aggregates.append(json.load(codecs.open(in_file, encoding="utf-8-sig")))
        if self.all_loaders_free():
            # stop unused nodes
            # for node_id in LOADER_NODE_IDS[2:]:
            for node_id in LOADER_NODE_IDS:
                COMM.send({
                    "name" : "stop",
                    "data" : {},
                }, dest=node_id
            )
            # start K-means 1-d clustering node
            # COMM.send({
            #     "name" : "do_1_dim_kmeans",
            #     "data" : {
            #         "dataset" : self.aggregates,
            #     }
            # }, dest=LOADER_NODE_IDS[0])
            # # start K-means 2-d clustering node
            # COMM.send({
            #     "name" : "do_2_dim_kmeans",
            #     "data" : {
            #         "dataset" : self.aggregates,
            #     }
            # }, dest=LOADER_NODE_IDS[1])
            # write out master dataset
            out_file = codecs.open(
                "%s/sendai-output/user_attention/full_data.json" % (
                    os.environ['SCRATCH'],
                ), "w",
                encoding="utf-8-sig"
            )
            json.dump(self.aggregates, out_file) #, indent=4)
            out_file.close()
            sys.exit()
            return self.STOP_LISTENING

