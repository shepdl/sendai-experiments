"""
    Generate word list
"""

import os
import codecs
import json

# in_file = codecs.open("../burst_detection/burst_words.txt", encoding="utf-8-sig")
in_file = codecs.open("../burst_detection/burst_words.txt", encoding="utf-8-sig")

bursts = {}
for line in in_file:
    span, _, words = line.split(":")
    # span = [int(s) for s in span.split(" to ")]
    #words = words.split("/")
    bursts[span] = [word.strip() for word in words.split("/")]

for i in range(26):
    out_file = codecs.open("%s/burst_list_%s.json" % (
        os.environ['SCRATCH'], i,), "w", encoding="utf-8-sig")
    json.dump(bursts, out_file)
    out_file.close()
# out_file = codecs.open("burst_list.py", "w", encoding="utf-8-sig")
# out_file.write("BURSTS = {")
# for key in sorted(bursts.keys()):
#     out_file.write(""" 
#         "%s" : "%s",
#     """ % (key, words.replace("\n", ""),) )
# 
# out_file.write("}")
# out_file.close()
# in_file.close()
# 
in_file = codecs.open("../burst_detection/word_burst_distribution.txt", encoding="utf-8-sig")
bursts = {}

# out_file = codecs.open("word_burst_distribution.py", "w", encoding="utf-8-sig")
# 
# out_file.write("SCORE_DISTRIBUTION = {")
for line in in_file:
    score, words = line.split(":", 1)
    for word in words.split(","):
        word = word.strip()
        bursts[word] = score
        # out_file.write("\"%s\": %s," % (word.strip(), score,))

# out_file.write("}")
for i in range(0, 26):
    out_file = codecs.open("%s/word_burst_distribution_%s.json" % (
        os.environ['SCRATCH'], i,
        ), "w", encoding="utf-8-sig")
    json.dump(bursts, out_file)
    out_file.close()
