import codecs
import numpy as np
import json
import csv
import os

# goal: show total number of on topic vs off topic tweets
# out file:
# username, cluster1 on, cluster1 off, cluster2 on, cluster2 off, total on, total off

def offset_in_range(offset, range):
    if int(offset) >= int(range[0]) and int(offset) >= int(range[1]):
        return True
    else: 
        return False

# load bursts
bursts = json.load(codecs.open("%s/burst_list_0.json" % (os.environ['SCRATCH'],), encoding="utf-8-sig"))
ranges = []
for burst in bursts:
    ranges.append(tuple([int(x) for x in burst.split(" to ")]))
print ranges
# load all data
data = json.load(codecs.open("%s/sendai-output/user_attention/full_data.json" % (os.environ['SCRATCH'],), encoding="utf-8-sig"))
# iterate through users and count tweets in each cluster
out_file = codecs.open("%s/sendai-output/user_attention/on_vs_off_topic.csv" % (os.environ['SCRATCH'],), "w", encoding="utf-8-sig")
headers = ["user",]
# for item in bursts.keys() + ["total",]:
for item in ranges: #+ ["total",]:
    headers.append("(%s, %s) on" % item)
    headers.append("(%s, %s) off" % item)

headers.append("total on")
headers.append("total off")

# headers = ["user",] + [col for col in ["%s on" % burst, "%s off" % burst,] for burst in bursts + ["total",]]
# headders = ["user",] + [col for ["%s on" % burst, "%s off" % burst,] in bursts for col in 
out_file = csv.DictWriter(out_file, headers)

out_file.writerow(dict([(h, h,) for h in headers]))

print "Analyzing users ..."
for user in data:
    line = dict([(h, 0,) for h in headers])
    line['user'] = user['username']
    # filter out any with too few results
    found_tweets = 0
    for offset, words in user['word_scores']:
        for range in ranges:
            if offset_in_range(int(offset), range):
                found_tweets += 1
                if len(words) > 0:
                    line['(%s, %s) on' % range] += 1
                    line['total on'] += 1
                else:
                    line['(%s, %s) off' % range] += 1
                    line['total off'] += 1
                break
        # else:
        #   line['total off'] += 1
    # if found_tweets > 2:
    out_file.writerow(line)

out_file.close()
