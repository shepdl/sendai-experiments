import codecs
import os
import json

bursts = json.load(codecs.open("%s/burst_list_0.json" % (os.environ['SCRATCH'],), encoding="utf-8-sig"))

full_data = json.load(codecs.open("%s/sendai-output/user_attention/full_data.json" % (os.environ['SCRATCH'],), encoding="utf-8-sig"))
tweet_scores =[]

offsets = bursts.keys()
for user in full_data:
    for offset, words in user['word_scores']:
        if offset in offsets:
            tweet_scores.append(sum([
                score for _, score in words
            ]))

tweet_average = 0.25 * (sum(tweet_scores) / len(tweet_scores) )

user_scores = []
out_file = codecs.open("results/on_off_by_percentage.csv", "w", encoding="utf-8-sig")
out_file.write("Username\tOn topic\tOff topic\n")

for user in full_data:
    on_topic = 0
    off_topic = 0
    for offset, words in user['word_scores']:
        if offset in offsets:
            tweet_score = sum([
                score for _, score in words
            ])
            if tweet_score > tweet_average:
                on_topic += 1
            else:
                off_topic += 1
    user_scores.append((user['username'], 
        on_topic, off_topic,
    ))
    out_file.write("%s\t%s\t%s\n" % (user['username'],
        on_topic, off_topic,))

out_file.close()
