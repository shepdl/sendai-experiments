import codecs
import numpy as np
import scipy.cluster.vq
import json
import os

clusters = 6

def do_1_dim_kmeans(dataset):
    in_data = []
    for user in dataset:
        # relevance_score = user['score']
        relevance_counts = len(user['relevance_counts'])
        relevance_score = sum(user['relevance_counts']) 
        in_data.append(relevance_score / relevance_counts)
    in_data = np.array(in_data)
    centroids, labels = scipy.cluster.vq.kmeans2(in_data, clusters)
    user_clusters = dict([(cluster_id, [],) 
        for cluster_id in range(clusters)]
    )
    for index in range(len(labels)):
        user_clusters[labels[index]].append(dataset[index])
    out_file = codecs.open(
        "%s/sendai-output/user_attention/1_dim_clusters.txt" % (
        os.environ['SCRATCH'],), "w", encoding="utf-8-sig"
    )
    user_cluster_keys = user_clusters.keys()
    for index in range(len(user_clusters)):
        centroid = centroids[index]
        key = user_cluster_keys[index]
        users = user_clusters[key]
        out_file.write("""Cluster %s (centroid: %s) \n""" % (
                index, centroid,
            )
        )
        out_file.write("Username\tRelevance Score\tTweets\n")
        for user in users:
            out_file.write("""%s\t%s\t%s\n""" % (
                user['username'], sum(user['relevance_counts']) / len(user['relevance_counts']), 
                user['relevance_counts'],
            ))
    out_file.close()

def do_2_dim_kmeans(dataset):
    clusters = 12
    in_data = []
    for user in dataset:
        relevance_counts = len(user['relevance_counts'])
        relevance_score = sum(user['relevance_counts']) 
        in_data.append( (relevance_counts, 
            relevance_score / relevance_counts,) )
    in_data = np.array(in_data)
    centroids, labels = scipy.cluster.vq.kmeans2(in_data, clusters)
    user_clusters = dict([(cluster_id, [],) 
        for cluster_id in range(clusters)])
    for index in range(len(labels)):
        user_clusters[labels[index]].append(dataset[index])
    out_file = codecs.open(
        "%s/sendai-output/user_attention/2_dim_clusters.txt" % (
        os.environ['SCRATCH'],), "w", encoding="utf-8-sig"
    )
    user_cluster_keys = user_clusters.keys()
    for index in range(len(user_clusters)):
        centroid = centroids[index]
        key = user_cluster_keys[index]
        users = user_clusters[key]
        out_file.write("""Cluster %s (centroid: %s) \n""" % (
                index, centroid,
            )
        )
        out_file.write("Username\tRelevance Score\tTweets\n")
        for user in users:
            out_file.write("""%s\t%s\t%s\n""" % (
                user['username'], sum(user['relevance_counts']) / len(user['relevance_counts']), 
                user['relevance_counts'],
            ))
    out_file.close()

in_data = json.load(codecs.open("%s/sendai-output/user_attention/full_data.json" % os.environ['SCRATCH'], encoding="utf-8-sig"))

# do_1_dim_kmeans(in_data)
do_2_dim_kmeans(in_data)
