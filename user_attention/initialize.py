#!/usr/bin/python
from mpi4py import MPI
from Aggregator import AggregatorController
from Loader import Loader

from config import LOADER_NODE_IDS, AGGREGATOR_NODE_ID

COMM = MPI.COMM_WORLD

if __name__ == "__main__":
    rank = COMM.Get_rank()
    if rank in LOADER_NODE_IDS:
        this_node = Loader()
    else:
        this_node = AggregatorController()
    this_node.run()

