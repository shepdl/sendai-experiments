#!/usr/bin/python
# coding=utf8
import sqlite3

from mpi4py import MPI

from utility import *
import project

# distribute terms to nodes

def find_term(term):
    term = term[0]
    data = project.make_local_copy(project.DATASET_BASE + "/20110314.db")
    input_db = sqlite3.connect(data)
    cursor = input_db.cursor()
    results = {}
    for row in cursor.execute("""
        SELECT strftime('%H:%M', date) AS minute,
            COUNT(*) AS total FROM tweets
            WHERE body LIKE ?
            GROUP BY strftime('%H:%M', date)
        """, (buffer("%%" + term + "%%"),)):
        results[row[0]] = row[1]
    input_db.close()
    os.remove(data)
    return {"term" : term, "results": results,}
            
if is_supervisor():
    project.make_temp_dir()

terms = "東京電力 東電 社長 副社長 役員 原発 原子力発電所 福島 停電 放射能 TEPCO".split(" ")
comm = MPI.COMM_WORLD
term = comm.scatter(terms),
results = comm.gather(
    find_term(term),
    root = 0
    )
# results = distribute(terms, find_term)

if is_supervisor():
    data = {}
    print results
    # results = flatten(results)
    for result in results:
        data[result["term"]] = result["results"]
    headers = ["time",] + terms
    column_headers = dict(map(lambda h: (h,h,), headers))
    rows = [column_headers,]
    for hour in range(24):
        for minute in range(60):
            key = "%02d:%02d" % (hour, minute,)
            row = { "time" : key, }
            for term in terms:
                row[term] = data[term][key] if key in data[term] else 0 
            rows.append(row)
    out_file = project.open_result_file("term-searches/2011-03-14.csv")(headers)
    out_file.writerows(rows)
