#!/usr/bin/python

import os
import sqlite3

import project
from utility import *

def node_file():
    return "total-tweets-per-user/node-%s.db" % (rank(),)
    
def total_tweets_per_user(filename):
    """
        Get count for all users in each file.
        Store these into a temporary SQLite file, which is always the same filename for each node.
        In the root node, insert all the values into one database and then summarize into another table.
    """
    input_db = sqlite3.connect(filename)
    input_cursor = input_db.cursor()
    output_db = project.open_temp_file(node_file())
    output_cursor = output_db.cursor()
    output_cursor.execute("""
        DROP TABLE IF EXISTS user_counts
    """)
    output_cursor.execute("""
        CREATE TABLE IF NOT EXISTS user_counts (id INTEGER PRIMARY KEY, username TEXT, total INTEGER)
    """)
    for row in input_cursor.execute("""
            SELECT username, COUNT(*) FROM tweets GROUP BY username
        """):
        output_cursor.execute("""
            INSERT INTO user_counts (username, total) VALUES(?,?)
        """, (user_from(row[0]), row[1],))
    output_db.commit()
    input_cursor.close()
    output_cursor.close()
    output_db.close()
    return node_file()


def main():
    if is_supervisor():
        try:
            os.mkdir(project.temp_file("total-tweets-per-user"))
        except OSError as ex:
            # The directory exists; don't worry
            pass
    result_files = distribute(project.DATASET, total_tweets_per_user)
    if is_supervisor():
        result_files = set(flatten(result_files))
        user_totals = {}
        output_db = project.open_result_file("total-tweets-per-user.db")
        output_cursor = output_db.cursor()
        output_cursor.execute(""" DROP TABLE IF EXISTS intermediary """)
        output_cursor.execute(""" DROP TABLE IF EXISTS results """)
        output_cursor.execute("""
            CREATE TABLE IF NOT EXISTS intermediary(username TEXT, total INTEGER)
        """)
        output_cursor.execute("""
            CREATE TABLE IF NOT EXISTS results(username TEXT, total INTEGER)
        """)
        for file in result_files:
            db = project.open_temp_file(file)
            cursor = db.cursor()
            for row in cursor.execute("""
                    SELECT username, SUM(total) AS total FROM user_counts GROUP BY username
                """):
                output_cursor.execute("""
                    INSERT INTO intermediary(username, total) VALUES (?, ?)
                """, (row[0], row[1])
                )
            output_db.commit()
            cursor.close()
        output_cursor.close()
        input_cursor = output_db.cursor()
        output_cursor = output_db.cursor()
        output_cursor.execute("""
            INSERT INTO results (username, total) 
            SELECT username, SUM(total) FROM intermediary GROUP BY username
        """)
        output_db.commit()
        output_db.close()
        input_cursor.close()

if __name__ == "__main__":
    main()
