# brute_force.py

import numpy as np

from shasha import threshold_test_func_for

def find_bursts(time_series, window_size_factor):
    threshold = threshold_test_func_for(time_series, factor=2)
    actual_bursts = []
    #for window_increment in np.arange(1, 8):
    # actual_window_size = window_increment * window_size_factor
    actual_threshold = threshold(window_size_factor)
    current_burst = []
    for starting_point in np.arange(len(time_series) / 
        # window_size_factor - window_increment * window_size_factor):
            window_size_factor):
        actual_starting_point = starting_point * window_size_factor
        window_average = np.average(time_series[actual_starting_point:
            # actual_starting_point + actual_window_size
            actual_starting_point + window_size_factor
        ])
        if window_average >= actual_threshold: 
            # threshold(window_size_factor): # threshold(actual_window_size):
            current_burst.append((actual_starting_point, window_average))
        else:
            if current_burst:
                actual_bursts.append(current_burst)
                current_burst = []
    return actual_bursts

def test_find_bursts():
    test_data = [[10,] * 10 + [20, 20,] + [30,] + [20,] * 5 + 
        [10,] * 10 + [20, 20,] + [30,] + [20,] * 5 + 
        [450, 880, 900, 880, 810, 210,] + [10,] * 10 + [20, 20,] + 
        [450, 340, 400,] + [10,] * 10 + 
        [20, 20,] + [30,] + [20,] * 5 + [20, 30, 20,] + 
        [450, 880, 900, 880, 810, 210,] + [10,] * 10 
        + [20, 20,] + [10,] * 3,] * 100
    test_data = [item for sublist in test_data for item in sublist]
    # print len(test_data)
    test_data = np.array(test_data, copy=True)
    print find_bursts(test_data, 4)

if __name__ == "__main__":
    test_find_bursts()