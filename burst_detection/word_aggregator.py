from mpi4py import MPI

from node import Node
from config import LOADER_NODE_IDS, BURSTER_NODE_IDS

COMM = MPI.COMM_WORLD

class WordAggregatorNode(Node):

    def __init__(self):
        super(WordAggregatorNode, self).__init__("WordAggregator")
        self.log("Aggregator running ...")
        self.words = {}
        self.available_bursters = dict([
            (node, True,) for node in BURSTER_NODE_IDS
        ])
        self.setup_dispatch_table({
            "send_words" : self.send_words,
            "burst_complete" : self.send_bursts,
            "loader_complete" : self.loader_complete,
        })
        self.working_loaders = dict([
            (node_id, True) for node_id in LOADER_NODE_IDS
        ])
        self.complete_loaders = dict([
            (node_id, False) for node_id in LOADER_NODE_IDS 
        ])
        self.bursted_words = {}

    def run(self):
        """
            Start loaders and then listen for messages.
        """
        self.start_loaders()
        super(WordAggregatorNode, self).run()

    def loader_complete(self, node_id):
        """
            Loader is totally complete; the program is getting ready to exit ...

            By this point, the burst detection is complete.
        """
        self.complete_loaders[node_id] = True
        if len(self.complete_loaders) == len(LOADER_NODE_IDS):
            for node in LOADER_NODE_IDS:
                COMM.send({
                    "name" : "STOP_LISTENING", 
                    "data" : { },
                }, dest=node)
            for node in BURSTER_NODE_IDS:
                COMM.send({
                    "name" : "STOP_LISTENING", 
                    "data" : { },
                }, dest=node)
        return False
                

    def start_loaders(self):
        for node in LOADER_NODE_IDS:
            COMM.send({
                "name" : "load_next", 
                "data" : {},
            }, dest=node)
            self.working_loaders[node] = True

    def send_words(self, date, words, node_id):
        """
            Message form: dictionary of date -> "", words -> "word" -> [(offset, count,)]
        """
        self.log("Aggregator received words ...")
        for word, counts in words.iteritems():
            self.words.setdefault(word, {})
            self.words[word][date] = counts
        # Then call a function to distribute code
        self.working_loaders[node_id] = False
        # True means the loader is running
        self.dispatch_to_bursters()


    def send_bursts(self, node_id, word, bursts):
        """
            Receive word bursts once the words have been completed.
        """
        self.log("Received bursts ...")
        self.bursted_words[word] = bursts
        self.available_bursters[node_id] = True
        new_words_sent = self.dispatch_to_bursters()
        free_bursters = [node for node, value in 
            self.available_bursters.iteritems() if value
        ]
        if not new_words_sent and len(self.words) == 0 and len(free_bursters) == len(BURSTER_NODE_IDS):
            self.log("All burst calculations complete; moving on to next level ...")
            print self.available_bursters
            self.start_loaders()
        # if self.loaders_are_complete():
        #    self.calculate_bursts()

    def loaders_are_complete(self):
        """
            Are the loaders complete?
        """
        self.log("All loaders complete.")
        still_working = []
        for node_id, status in self.complete_loaders.iteritems():
            if not status:
                still_working.append(node_id)
        return len(still_working) == 0

    def calculate_bursts(self):
        """
            Start calculating the bursts.
        """
        # find random node and have it calculate the bursts
        pass

    def dispatch_to_bursters(self):
        """
            If there are any free bursters, send words to them.
            Called from both receive_words() and receive_bursts()
        """
        free_bursters = [node for node, value in 
            self.available_bursters.iteritems() if value
        ]
        if len(free_bursters) == 0:
            self.log("No free bursters.")
        elif len(free_bursters) > 0:
            for node in free_bursters:
                word_to_send = None
                dates = []
                for (word, dates) in self.words.iteritems():
                    if len(dates) == 25:
                        print "Sending %s" % (word_to_send,)
                        word_to_send = word
                        break
                self.log("Words have %s reporting" % (len(dates),))
                if word_to_send:
                    self.log("%s free bursters found." % (len(free_bursters),))
                    self.log("Sending %s to %s" % (word_to_send, node,))
                    COMM.send({
                        "name" : "find_bursts", 
                        "data" : {
                            "word" : word_to_send,
                            "time_series" : self.words[word_to_send],
                        },
                    }, dest=node)
                    del self.words[word_to_send]
                    self.log("%s words remaining ..." % (len(self.words),))
                    self.available_bursters[node] = False
                else:
                    self.log("Could not find a word that was both "
                        + "complete, or a burster to process it; "
                        + "%s words in queue and %s bursters." % (
                            len(self.words), len(free_bursters),
                    ))
        # return a truthy value if we dispatched additional jobs
        else:
            self.log("Should never happen ...")
        return word_to_send
