"""
    Shasha.py: implementation Shasha's burst detection algorithm
    See http://www.cs.nyu.edu/cs/faculty/shasha/papers/burst.d/burst.pdf
"""
import numpy as np

def find_bursts(word, time_series, window_size, limit=10):
    """
        Entry point.
    """
    # to calculate the correct range, we would have to find the correct
    # number of powers of 2, which we do by dividing the total length of the
    # time series, then taking the log2 of that quantity
    # or does this already happen in potential_subsequences?
    #window_exp_max = np.log2(len(time_series) / float(window_size))
    # for window_size in range(window_size, window_size * limit, window_size):
    # for window_exp in np.arange(window_exp_max):
    # this_window_size = window_size * 2 ** window_exp
    this_window_size = window_size
    new_time_series = window_sums(time_series, this_window_size )
    # print len(new_time_series)
    bursts = potential_subsequences(
        new_time_series,
        build_shifted_wavelet_tree(new_time_series),
        this_window_size,
        threshold_test_func_for(new_time_series)
    )
        # bursts = potential_subsequences(
        #     time_series,
        #     build_shifted_wavelet_tree(time_series),
        #     window_size,
        #     threshold_test_func_for(time_series)
        # )
    return (word, bursts,)

def build_shifted_wavelet_tree(series):
    """
    Build a shifted wavelet tree.

    Inputs:
        x: time series (1-dimension array-like)

    Returns:
        A shifted wavelet tree: a 2-dimensional np.array-like of shifted wavelets.
        The first axis is the level number; the second axis is the values in each window.
        This means, of course, that each axis is about 1/2 as large as the previous.
    """
    # TODO: is it correct to ceil here to compensate for lengths
    # that are not powers of 2?
    # TODO: replace all 5's with window_size
    # TODO: what is relationship between SWT and window size?
    # Isn't SWT supposed to only preserve one window size thorughout
    # the hierarchy?
    a = np.ceil(np.log2(len(series)))
    # swt = np.zeros((a, len(series)))
    swt_arrays = [np.array(series),]
    for i in np.arange(np.log2(len(series)), -1, step=-1):
        # print "Array size: %d" % (np.ceil(2 ** i),)
        swt_arrays.append(np.zeros(np.ceil(2 ** i)))
    swt = np.array(swt_arrays)
    # print swt
    for i in np.arange(a):
        print i
        # print "SWT[i] %s" % len(swt[i])
        # print "series len: %s" % len(series)
        # print np.arange(len(series) - 1, step=2)
        for j in np.arange(len(series) - 1, step=2):
            # print "Swt[%s][%s]: %s" % (i, j, swt[i][j],)
            try:
                swt[i][j] = series[j] + series[j + 1]
            except IndexError as ex:
                # print i
                print j
                # print len(series)
                # print len(swt[i])
                print swt
                print swt[i][j]
        downsampled_series = np.zeros(len(swt[i]) / 2)
        # print "DS len: %s" % len(downsampled_series)
        for j in range(len(swt[i]) / 2):
            series[j] = swt[i][2 * j - 1]
        # series = downsampled_series
    # print "SWT last length: %s" % (len(swt[len(swt) - 1]))
    print swt
    return swt

def potential_subsequences(time_series, shifted_wavelet_tree, 
        window_size, threshold):
    """
        Identify bursts, or at least possible subsequences that might contain bursts.

        Inputs:
        time_series: original time series, not the sliding window tree
        shifted_wavelet_tree: the tree
        window_size: the desired window size
        threshold: a threshold function for a given window size that determines if the 
            given window (or values in the given window?) fall into burst category
    """
    i = int(np.ceil(np.log2(window_size)))
    bursts = []
    # iterate through shifted wavelenght tree at the level of the current window size
    # TODO: what's the relationship between window size and level? That seems implict
    # here.
    # window size is always window_size * 2 ** tree level
    # so where do we iterate through the correct window sizes? here or
    # in find_bursts?
    print shifted_wavelet_tree
    for j in range(len(shifted_wavelet_tree[i + 1])):
        # print "%s, %s" % (j, shifted_wavelet_tree[i + 1][j],)
        if shifted_wavelet_tree[i + 1][j] > threshold(window_size):
            # TODO: why is c larger than the length of time_series?
            for c in range((j - 1) * 2 ** i + 1, j * 2 ** i):
                print "Threshold: %s" % (threshold(window_size),)
                print time_series
                print len(time_series)
                print c
                print c - 1 + 2 ** i
                print time_series[c:c - 1 + 2 ** i]
                print window_size
                y = sum(time_series[c:c - 1 + 2 ** i])
                print "Sum: %s " % (y,)
                sys.exit()
                if y > threshold(window_size):
                    print "Over threshold ..."
                    # do detailed search in time_series[c:c-1 + 2 ** i] here ...
                    # possible_bursts = map(lambda w: w if w > threshold(w) 
                    # else 0, time_series[c:c - 1 + 2 ** i])
                    possible_bursts = [w if w > threshold(w) else 0 
                        for w in time_series[c:c - 1 + 2 ** i]
                    ]
                    current_burst = []
                    # separate out possible_bursts by effectively 
                    # 'splitting' on 0s
                    for number in possible_bursts:
                        if number > 0:
                            current_burst.append(number)
                        else:
                            if current_burst != []:
                                bursts.append(current_burst)
                                current_burst = []
    return bursts

def window_sums(series, window_size):
    """
        Compute sum of windows
    """
    series = np.array(series, copy=True)
    series.resize(len(series) + (window_size - len(series) % window_size))
    # series[initial_size:]
    # series = series.concatenate(np.zeroes((len(series) % window_size),))
    return series.reshape(
        len(series) / window_size, window_size
    ).sum(axis=1)

# def threshold_test_func_for(series, window_size, factor=8):
def threshold_test_func_for(series, factor=8):
    """
        Generate threshold function
    """
    series = np.array(series, copy=True)
    def ttf(window_size):
        """
            Inner function
        """
        series.resize(len(series) + (window_size - len(series) % window_size))
        # summarize each row
        segmented_by_window_size = series.reshape(
            len(series) / window_size, window_size
        )
        std_dev = np.std(segmented_by_window_size)
        mean = np.mean(segmented_by_window_size)
        return (mean + factor * std_dev)
    return ttf

def test_swt():
    test_data = np.arange(100)
    print build_shifted_wavelet_tree(test_data)

def test_shasha():
    test_data = [[10,] * 10 + [20, 20,] + [30,] + [20,] * 5 + 
        [10,] * 10 + [20, 20,] + [30,] + [20,] * 5 + 
        [450, 880, 900, 880, 810, 210,] + [10,] * 10 + [20, 20,] + 
        [450, 340, 400,] + [10,] * 10 + 
        [20, 20,] + [30,] + [20,] * 5 + [20, 30, 20,] + 
        [450, 880, 900, 880, 810, 210,] + [10,] * 10 
        + [20, 20,] + [10,] * 3,] * 100
    test_data = [item for sublist in test_data for item in sublist]
    # print len(test_data)
    test_bursts = np.array(test_data, copy=True)
    print find_bursts("test", test_bursts, 5, 5)

if __name__ == "__main__":
    #test_swt()
    #test_shasha()
    pass