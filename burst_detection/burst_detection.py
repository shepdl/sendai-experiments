import numpy as np

# from node import Node

TEST_BURSTS = (
    [10,] * 10 + [20, 20,] + [30,] + [20,] * 5 + [10,] * 10 + [20, 20,] + [30,] + [20,] * 5 + 
    [450, 880, 900, 880, 810, 210,] + [10,] * 10 + [20, 20,] + [450, 340, 400,] + [10,] * 10 + 
    [20, 20,] + [30,] + [20,] * 5 + [20, 30, 20,] + [450, 880, 900, 880, 810, 210,] + [10,] * 10 
    + [20, 20,] + [10,] * 3
)

# class BurstFinder(Node):
#     """
#         The general idea of discovering bursts is to search over a time series 
#         (one time series) in small increments and then compute the average of 
#         the values within each increment to see if that's a burst. Because 
#         bursts by definition have a variable size, we need to check all possible
#         windows. What Shasha's algorithm provides is a better way to check the 
#         size of windows, which is more efficient than a brute force way. Building
#         a shifted wavelet tree allows computing the size of a burst very qucikly. 

#         The process of checking for bursts will be to take a time series, go
#         through it, and then return a list of bursts, which take the form of a 
#         tuple: (word, start, end, [values,],)
#     """

#     def __init__(self):
#         super(BurstFinder, self).__init__("BurstFinder")
#         self.setup_dispatch_table({
#             "find_bursts" : self.find_bursts,
#         })


class ShashaDiscovery(object):
    """
        ShashaDiscovery uses Shasha's algorithm to discover bursts.
    """
    def find_bursts(self, word, time_series):
        for window_size in range(0, 600, 30):
            new_time_series = self.window_sums(time_series, window_size)
            bursts = self.potential_subsequences(
                new_time_series,
                self.build_shifted_wavelet_tree(new_time_series),
                window_size,
                self.threshold_for(new_time_series)
            )
        # TODO: how to return this?
        return (word, bursts,)

    def build_shifted_wavelet_tree(self, series):
        """
        Build a shifted wavelet tree.

        Inputs:
            x: time series (1-dimension array-like)

        Returns:
            A shifted wavelet tree: a 2-dimensional np.array-like of shifted wavelets.
            The first axis is the level number; the second axis is the values in each window.
            This means, of course, that each axis is about 1/2 as large as the previous.
        """
        a = np.log2(len(series))
        swt = np.zeros((a, len(series)))
        for i in range(a):
            for j in range(0, len(series) - 1, 2):
                swt[i][j] = series[j] + series[j + 1]
            for j in range(len(swt[i]) / 2):
                series[j] = swt[i][2 * j - 1]
        return swt

    def potential_subsequences(self, time_series, shifted_wavelet_tree, window_size, threshold):
        """
            Identify bursts, or at least possible subsequences that might contain bursts.

            Inputs:
            time_series: original time series, not the sliding window tree
            shifted_wavelet_tree: the tree
            window_size: the desired window size
            threshold: a threshold function for a given window size that determines if the 
                given window (or values in the given window?) fall into burst category
        """
        i = np.ceil(np.log2(window_size))
        bursts = []
        for j in range(len(shifted_wavelet_tree[i + 1])):
            if shifted_wavelet_tree[i + 1][j] > threshold(window_size):
                for c in range((j - 1) * 2 ** i + 1, j * 2 ** i):
                    y = sum(time_series[c:c - 1 + 2 ** i])
                    if y > threshold(window_size):
                        # do detailed search in time_series[c:c-1 + 2 ** i] here ...
                        possible_bursts = map(lambda w: w if w > threshold(w) else 0, time_series[c:c - 1 + 2 ** i])
                        current_burst = []
                        # separate out possible_bursts by effectively 'splitting' on 0s
                        for number in possible_bursts:
                            if number > 0:
                                current_burst.append(number)
                            else:
                                if current_burst != []:
                                    bursts.append(current_burst)
                                    current_burst = []
        return bursts

    def window_sums(self, series, window_size):
        return series.reshape(
            len(series) / window_size, window_size
        ).sum(axis=1)

    def threshold_test_func_for(self, series, window_size, factor=8):
        series = np.array(series, copy=True)
        def tf(window_size):
            # summarize each row
            segmented_by_window_size = series.reshape(
                len(series) / window_size, window_size
            )
            std_dev = np.std(segmented_by_window_size)
            mean = np.mean(segmented_by_window_size)
            return value > (mean + factor * std_dev)
        return tf

    def threshold_for(self, series):
        """
            Create a function that determines if a value qualifies as a burst
        """
        # take absolute average deviation of series
        std_dev = np.std(series)
        mean = np.mean(series)
        def qualifier_func(value):
            return ((value - mean) / std_dev) >= 2
        return qualifier_func


