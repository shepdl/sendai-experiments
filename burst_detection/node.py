from mpi4py import MPI
import datetime

from communication import message_queue

COMM = MPI.COMM_WORLD

class Node(object):

    def __init__(self, name=""):
        self.STOP_LISTENING = "STOP_LISTENING"
        self.name = name
        self.listening = True

    def setup_dispatch_table(self, table):
        self.dispatch_table = table

    def run(self):
        self.log("Starting message queue.")
        self.listening = True
        self.listen_for_messages()

    def listen_for_messages(self, stop_messages=[]):
        for message in message_queue():
            name = message["name"]
            self.log("Message: " + message["name"])
            if name == "stop" or name == self.STOP_LISTENING or name in stop_messages:
                self.listening = False
            if not self.listening:
                break
            if name in self.dispatch_table:
                result = self.dispatch_table[name](**message["data"])
            else:
                self.log("Call error: unhandled message %s" % (name,))
            if name == self.STOP_LISTENING or result == self.STOP_LISTENING:
                self.listening = False
                break

    def log(self, message):
        print "[%s Node %s (%s)] %s" % (
            datetime.datetime.now(),
            COMM.Get_rank(),
            self.name,
            message,
        )

class API(object):

    # TODO: load dispatch table from corresponding object

    def __init__(self, instance):
        self.dispatch_table = instance.dispatch_table

    def send(self, destination_node_id, message):
        MPI.send(destination_node_id, message)
