# loader.py

import os
import re
import string

import sqlite3

from config import AGGREGATOR_NODE_ID
from node import Node
from words_to_track import WORDS_TO_TRACK as WTS
from utility import *

from mpi4py import MPI

COMM = MPI.COMM_WORLD


class Loader(Node):
    """
        Loads words from database
    """
    def __init__(self, filename):
        super(Loader, self).__init__("Loader")
        self.filename = filename
        self.setup_dispatch_table({
            "load_next" : self.load_next,
        })
        self.counter = 0
        self.limit = 49408
        self.increment = 2500

    def load_next(self):
        """
            Main run method
        """
        self.log("Checking for words starting at %s" % (self.counter,))
        if self.counter >= self.limit:
            COMM.send({
                "name" : "loader_complete",
                "data" : {
                    "node_id" : COMM.Get_rank(),
                }
            }, dest=AGGREGATOR_NODE_ID[0])
            return
        increment = self.increment
        connection = sqlite3.connect("%s/sendai-segmented/%s.db" % (
                os.environ['SCRATCH'],
                self.filename,
            )
        )
        cursor = connection.cursor()
        #for start in range(0, 50000, increment):
        start = self.counter + self.increment
        if start + increment >= 49408:
            words_to_seek = WTS[start:]
        else:
            words_to_seek = WTS[start:start + increment]
        # Remove 'blank' words
        found_words = dict((word, {}) for word in words_to_seek if word)
        for row in cursor.execute(""" SELECT date, segmented_body
                FROM tweets WHERE segmented_body != ""
            """):
            tweeted_at, body = row
            if not body:
                continue
            body = re.sub("[\\d%s]" % (string.punctuation), " ", body)
            for date, word in [(tweeted_at, word,) for word 
                    in body.split() if word.lower() in found_words]:
                word = word.lower()
                # trim date and anything under 10 minutes from date
                #date = date.split(" ")[1][0:4] + "0"
                year, month, day = date.split(" ")[0].split("-")
                time_component = date.split(" ")[1].split(":")
                hour = time_component[0]
                minutes = time_component[1]
                offset = (
                    (int(day) - 9 if month == "03" else 
                        int(day) + 22) * 1440 +
                    (int(hour) * 60) +
                    int(minutes)
                )
                # found_words[word].setdefault(date, 0)
                # found_words[word][date] += 1
                found_words[word].setdefault(offset, 0)
                found_words[word][offset] += 1
        # once we have loaded all the words, pass them on to the aggregator
        # pass to aggregator
        # Form is {"date" : "date", "words" : {word: [(offset, count)]}}
        self.log("Done; sending to aggregator")
        COMM.send({
            "name" : "send_words",
            "data" : {
                "date" : date_from(self.filename),
                "words" : found_words,
                "node_id" : COMM.Get_rank(),
            }
        }, dest=AGGREGATOR_NODE_ID[0])
        self.counter += self.increment
            # maybe wait here until we get the signal that the aggregator is ready to pass on the tuples to the
            # burst detectors, so that the aggregator doesn't get overloaded with terms from the first two days
