""" 
    BurstDetectorNode
"""

import codecs
import os
import json

import numpy as np
from mpi4py import MPI

COMM = MPI.COMM_WORLD

from config import AGGREGATOR_NODE_ID
from node import Node



class BurstDetectorNode(Node):
    """
        Do burst detection.
    """
    def __init__(self):
        """
            Initialize burst detector
        """
        super(BurstDetectorNode, self).__init__("BurstDetectorNode")

        self.setup_dispatch_table({
            "find_bursts" : self.find_bursts
        })

    def find_bursts(self, word, time_series, window_size_factor=30):
        """
            Do the actual burst finding
        """
        self.log("Searching for bursts in %s" % (word,))
        # words = [word_list for date, word_list in 
        # self.words[word].iteritems()]
        words = [word_list for _, sublist in time_series.iteritems() 
            for word_list in sublist.iteritems()
        ]
        # words = [word_list for _, sublist in time_series.iteritems()]
        time_and_word_counts = sorted(words, key=lambda x: x[0])
        time_series = np.zeros(25 * 24 * 60)
        # Loop necessary because there may be gaps in the word series
        for time, word_count in time_and_word_counts:
            time_series[time] = word_count
        # actual_bursts = find_bursts(time_series, window_size_factor)
        threshold = threshold_test_func_for(time_series, factor=2)
        actual_bursts = []
        #for window_increment in np.arange(1, 8):
        # actual_window_size = window_increment * window_size_factor
        actual_threshold = threshold(window_size_factor)
        current_burst = []
        for starting_point in np.arange(len(time_series) / 
            # window_size_factor - window_increment * window_size_factor):
                window_size_factor):
            actual_starting_point = starting_point * window_size_factor
            window_average = np.average(time_series[actual_starting_point:
                # actual_starting_point + actual_window_size
                actual_starting_point + window_size_factor
            ])
            if window_average >= actual_threshold: 
                # threshold(window_size_factor): 
                # threshold(actual_window_size):
                current_burst.append((actual_starting_point, window_average))
            else:
                if current_burst:
                    actual_bursts.append(current_burst)
                    current_burst = []
        # actual_bursts = np.array(actual_bursts)
        self.log("Writing to file ...")
        out_file = open("%s/sendai-output/word_bursts/%s.csv" % (
            os.environ['SCRATCH'], word,
        ), "w")
        # actual_bursts.tofile(out_file)
        json.dump(actual_bursts, out_file)
        out_file.close()
        self.log("Returning to aggregator")
        COMM.send({
            "name" : "burst_complete",
            "data" : {
                "node_id" : COMM.Get_rank(),
                "word" : word,
                "bursts" : actual_bursts,
            }
        }, dest=AGGREGATOR_NODE_ID[0])

def find_bursts(time_series, time_series_factor=8):
    # words = [word_list for _, sublist in time_series.iteritems()]
    time_and_word_counts = sorted(time_series, key=lambda x: x[0])
    time_series = np.zeros(25 * 24 * 60)
    # Loop necessary because there may be gaps in the word series
    for time, word_count in time_and_word_counts:
        time_series[time] = word_count
    threshold = threshold_test_func_for(time_series, factor=2)
    actual_bursts = []
    #for window_increment in np.arange(1, 8):
    # actual_window_size = window_increment * window_size_factor
    actual_threshold = threshold(window_size_factor)
    current_burst = []
    for starting_point in np.arange(len(time_series) / 
        # window_size_factor - window_increment * window_size_factor):
            window_size_factor):
        actual_starting_point = starting_point * window_size_factor
        window_average = np.average(time_series[actual_starting_point:
            # actual_starting_point + actual_window_size
            actual_starting_point + window_size_factor
        ])
        if window_average >= actual_threshold: 
            # threshold(window_size_factor): 
            # threshold(actual_window_size):
            current_burst.append((actual_starting_point, window_average))
        else:
            if current_burst:
                actual_bursts.append(current_burst)
                current_burst = []
    return actual_bursts

def threshold_test_func_for(series, factor=8):
    """
        Generate threshold function
    """
    series = np.array(series, copy=True)
    def ttf(window_size):
        """
            Inner function
        """
        series.resize(len(series) + (window_size - 
            len(series) % window_size))
        # summarize each row
        segmented_by_window_size = series.reshape(
            len(series) / window_size, window_size
        )
        std_dev = np.std(segmented_by_window_size)
        mean = np.mean(segmented_by_window_size)
        return (mean + factor * std_dev)
    return ttf


class BurstAggregatorNode(Node):
    def __init__():
        super(BurstDetectorNode, self).__init__("BurstDetectorNode")

        self.setup_dispatch_table({
            "burst_complete" : self.burst_complete
        })
        self.bursts = []

    def burst_complete(self, word, bursts):
        for time, count in bursts:
            self.bursts.setdefault(time, {"words": [], "count": 0})
            self.bursts[time]["words"].append(word)
            self.bursts[time]["count"] += count

