# meta burst detection: analyze list of bursts
# encoding=utf-8

import os
import glob
import json
import codecs

import numpy as np

window_size_factor = 1 # 30

def threshold_test_func_for(series, factor=8):
    """
        Generate threshold function
    """
    series = np.array(series, copy=True)
    def ttf(window_size):
        """
            Inner function
        """
        series.resize(len(series) + (window_size - 
            len(series) % window_size))
        # summarize each row
        segmented_by_window_size = series.reshape(
            len(series) / window_size, window_size
        )
        std_dev = np.std(segmented_by_window_size)
        mean = np.mean(segmented_by_window_size)
        return (mean + factor * std_dev)
    return ttf


# Process: go through existing word bursts as one time series.
burst_series = {}
for in_file in glob.glob("%s/sendai-output/word_bursts/*.csv" % (os.environ['SCRATCH'],)):
    in_file = in_file.decode("utf-8")
    word = in_file.split(".")[0].split("/")[-1]
    data = json.load(open(in_file))
    if len(data) == 0:
        continue
    this_item = []
    amplitudes = []
    for burst in data:
        for point in burst:
            print point
            time_index, amplitude = point
            this_item.append({
                "time" : time_index,
                "amplitude" : int(amplitude),
            })
            amplitudes.append(amplitude)
    # Eliminate words where there's very little variation
    # Either they're rarely mentioned, or they're mentioned so
    # frequently and so consistently that they rarely show up.
    if max(amplitudes) - min(amplitudes) >= 100:
        for burst in this_item:
            burst_series.setdefault(int(burst["time"]), {
                "time" : burst["time"], 
                "amplitude" : 0,
                "words": [],
            })
            burst_series[burst["time"]]["amplitude"] += int(burst["amplitude"])
            burst_series[burst["time"]]["words"].append(word)
        

# Merge these by measuring amplitude: adding all words together so that each 
# minute has one burst, and then link these together.
# Filter out any "bursts" where the amplitude is less than 10 or so (at least, 
# as an initial experiment.)
# Make triples like this: (time, amplitude, [word,],)

<<<<<<< HEAD
bursts_list = [(0,0,)] * (25 * 24 * 60)
=======
bursts_list = [0,] * (25 * 24 * 60)
print bursts_list
>>>>>>> 293185d16453a27f8cd8e2ce1d2b30ae8ad28f2a
time_series = [0,] * (25 * 24 * 60)
for time, data in burst_series.iteritems():
    bursts_list[time] = (data["amplitude"], data["words"],)
    time_series[time] = data["amplitude"]

# Run burst detection on this new time series.

# Loop necessary because there may be gaps in the word series
# time_series = [0] * len(bursts_list)
# for time, word_count, words in bursts_list:
#     time_series[time] = word_count
# actual_bursts = find_bursts(time_series, window_size_factor)
threshold = threshold_test_func_for(time_series, factor=2)
actual_bursts = []
#for window_increment in np.arange(1, 8):
# actual_window_size = window_increment * window_size_factor
actual_threshold = threshold(window_size_factor)
current_burst = []


for starting_point in np.arange(len(bursts_list) / 
    # window_size_factor - window_increment * window_size_factor):
        window_size_factor):
    actual_starting_point = starting_point * window_size_factor
    # window_average = np.average(bursts_list[actual_starting_point:
    #     # actual_starting_point + actual_window_size
    #     actual_starting_point + window_size_factor
    # ])
    this_window_words = []
    this_window_sum = 0
    for amplitude, words in bursts_list[actual_starting_point:
            actual_starting_point + window_size_factor]:
        if amplitude == 0:
            continue
        this_window_sum += amplitude
        this_window_words += words

    this_window_words = list(set(this_window_words))
    window_average = this_window_sum / float(window_size_factor)
    if window_average >= actual_threshold: 
        # threshold(window_size_factor): 
        # threshold(actual_window_size):
        current_burst.append((actual_starting_point, window_average, 
            this_window_words,))
    else:
        if current_burst:
            start = (current_burst[0][0])
            average = sum([average for _, average, _ in current_burst]) / len(current_burst)
            words = list(set([word for _, _, words in current_burst for word in words]))
            actual_bursts.append((start, average, words, current_burst[-1][0]))
            # actual_bursts.append(current_burst)
            current_burst = []

# Go through list of new bursts: print out time series of words that burst 
# together,  with time stamp, in this format:
# time, amplitude, words

out_file = codecs.open("burst_words.txt", "w", encoding="utf-8-sig")
burst_counter = 0
for burst in actual_bursts:
    # this_burst_start = burst[0][0]
    # this_burst_last_point = burst[-1][0]
    # this_burst_words = list(set([word for start, amplitude, words in burst for word in words]))
    # this_burst_amplitude = sum([amplitude for start, amplitude, words in burst])
    this_burst_start, this_burst_amplitude, this_burst_words, this_burst_last_point = burst
    out_file.write(u"%s to %s:%s:%s \n" % (this_burst_start, 
        this_burst_last_point, this_burst_amplitude, 
        u"/".join(this_burst_words),
        )
    )

out_file.close()
