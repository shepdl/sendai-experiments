#!/usr/bin/python
from mpi4py import MPI
from loader import Loader
from word_aggregator import WordAggregatorNode
from burst_detector import BurstDetectorNode

from config import LOADER_NODE_IDS, AGGREGATOR_NODE_ID, BURSTER_NODE_IDS

COMM = MPI.COMM_WORLD

if __name__ == "__main__":
    rank = COMM.Get_rank()
    if rank in LOADER_NODE_IDS:
        file_date = (["201103%02d" % (x + 9,) for x in range(23)] + 
            ["20110401", "20110402",])[COMM.Get_rank()]
        this_node = Loader(file_date)
    elif rank in AGGREGATOR_NODE_ID:
        this_node = WordAggregatorNode()
    else:
        this_node = BurstDetectorNode()
    this_node.run()
