import sys
import csv

to_convert_file = csv.DictReader(open(sys.argv[1]))
totals_file = csv.DictReader(open(sys.argv[2]))

totals = {}
for line in totals_file:
    date = line["day"]
    del line["day"]
    totals[date] = line

headers = dict(map(lambda k: (k,k,), ["day",] + line.keys()))
output = csv.DictWriter(open(sys.argv[3], "w"), headers)
output.writerow(headers)

for line in to_convert_file:
    row = {"day" : line["day"]}
    del line["day"]
    for k, v in line.iteritems():
        row[k] = 100 * (float(v) / float(totals[row["day"]][k]))
    output.writerow(row)
