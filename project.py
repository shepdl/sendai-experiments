# -*- coding: utf-8 -*- 
import glob 
import os 
import sys
import shutil
import codecs

import sqlite3
import csv


from utility import *

DATASET_BASE = os.path.join(os.environ["SCRATCH"], "sendai-data")
DATASET_PATH = os.path.join(os.environ["SCRATCH"], "sendai-data", "*.db")
DATASET_SEGMENTED_PATH = os.path.join(os.environ["SCRATCH"], "sendai-segmented", "*.db")
# DATASET_PATH = "data/*.db" #  os.path.join("$SCRATCH", "sendai-experiments", "*.db")
DATASET = glob.glob(DATASET_PATH)
DATASET_SEGMENTED = glob.glob(DATASET_SEGMENTED_PATH)

SCRATCH_BASE = os.path.join(os.environ["SCRATCH"], "temp-data",)
if not os.path.exists(SCRATCH_BASE):
    os.mkdir(SCRATCH_BASE, 0744)

RESULT_PATH_BASE = os.path.join(os.environ["HOME"], "sendai-experiments", "results")


# The NULL TAG, used to represent when a message has no hashtags
# This is guaranteed never to conflict with any hashtags in this
# dataset as "-" characters are not allowed in hashtags and so
# are excluded by the tags_from algorithm.
NULL_TAG = "NULL-TAG"

JOB_NAME = sys.argv[0].split("/")[-1]

def make_temp_dir(name=None):
    try:
        if name is None:
            name = JOB_NAME
        os.mkdir(os.path.join(SCRATCH_BASE, JOB_NAME))
    except OSError as ex:
        pass

def make_local_copy(src_filename):
    dest_filename = os.path.join(SCRATCH_BASE, JOB_NAME, "%s.%s" % (src_filename.split("/")[-1], rank(),))
    if not os.path.exists(dest_filename):
        shutil.copyfile(src_filename, dest_filename)
    # os.remove(dest_filename)
    return dest_filename
    
def needs_data_copy(fn):
    return make_local_copy

def temp_file(*args):
    return os.path.join(SCRATCH_BASE, *args)

def open_temp_file(*parts, **kwargs):
    mode = "w"
    if kwargs:
        if "mode" in kwargs:
            mode = kwargs["mode"]

    name = temp_file(SCRATCH_BASE, *parts)
    if len(parts) > 1:
        if parts[:1].find(":") != -1:
            dir_name = parts[0:-1]
        else:
            dir_name = parts
        os.makedirs(os.path.join(*dir_name))
    filename, extension = parts[-1].split(".")
    interface = { 
        "csv" : lambda x: lambda headers: csv.DictWriter(codecs.open(x, mode, encoding="utf-8-sig"), headers), 
        "db" : sqlite3.connect 
    }[extension]
    return interface(result_file(name))
    
def result_file(name):
    filename = os.path.join(RESULT_PATH_BASE, name)
    # if "/" in name:
    #     dir = os.path.join(RESULT_PATH_BASE, *name.split("/")[0:-1])
    #     os.makedirs(dir)
    return filename

def open_result_file(name, mode=None):
    if mode is None:
        mode = "w"
    elif mode == "r":
        mode = ""
    filename, extension = name.split(".")
    if extension == "csv":
        return lambda headers: csv.DictWriter(codecs.open(result_file(name), mode, encoding="utf-8-sig"), headers)
    elif extension == "db":
        return sqlite3.connect(result_file(name))
    return
    # TODO: delete
    interface = {
        "csv" : lambda x: lambda headers: csv.DictWriter(open(x, mode), headers), 
        "db" : sqlite3.connect
    }[extension]
    return interface(result_file(name))
