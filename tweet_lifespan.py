#!/usr/bin/python
# tweet_lifespan.py
# calculate the lifespan of popular tweets
#
#

import sqlite3

import project
from utility import *


def tweet_lifespan(filename):
	file = sqlite3.connect(filename)
	cursor = file.cursor()
	date = date_from(filename)
	output_file = project.open_result_file("tweet-lifespans-" + date + ".db")
	results_file = output_file.cursor()
	results_file.execute("""CREATE TABLE IF NOT EXISTS message_lifespan 
		(id INTEGER PRIMARY KEY, started_at TIMESTAMP, ended_at TIMESTAMP, 
			message TEXT, lifespan FLOAT)
		""")
	for row in cursor.execute("""SELECT body, 
			MIN(date) AS started,
			MAX(date) AS ended,
			julianday(MAX(date)) - julianday(MIN(date)) AS lifespan
				FROM tweets GROUP BY body HAVING COUNT(*) > 1"""):
		results_file.execute("""INSERT INTO message_lifespan 
					(message, started_at, ended_at, lifespan) VALUES (?, ?, ?, ?) """,
				(row[0], row[1], row[2], row[3])
			)
	output_file.close()

def main():
	distribute(project.DATASET, tweet_lifespan)

if __name__ == "__main__":
	main()
