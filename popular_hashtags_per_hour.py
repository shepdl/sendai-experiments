import sqlite3
import glob

from utility import *

# iterate over files
output = sqlite3.connect("results/hashtag-top-20.db")
output_cursor = output.cursor()
output_cursor.execute("""
    DROP TABLE IF EXISTS results
""")
output_cursor.execute("""
    CREATE TABLE IF NOT EXISTS results (time DATE, hashtag TEXT, total INTEGER);
""")
output.commit()
for db_file in glob.glob("results/hashtag-counts/*.db"):
    if db_file ==  "results/hashtag-counts/hashtag-counts-master.db":
        continue
    db = sqlite3.connect(db_file)
    print db_file
    # iterate over hours
    date = date_from(db_file)
    date = date[0:4] + "-" + date[4:6] + "-" + date[6:]
    cursor = db.cursor()
    # for hour in range(24):
    #     time = "%s %s:00:00" % (date, hour,)
    for row in cursor.execute("""SELECT hour, tag, total FROM results
        -- WHERE hour = ? 
        ORDER BY total DESC
        LIMIT 20 """ #, (hour,)
        ):
        output_cursor.execute(
            """
                INSERT INTO results (time, hashtag, total) VALUES(?, ?, ?)
            """, (date, row[1], row[2],)
        )
    output.commit()
    cursor.close()

