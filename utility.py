import glob
import os
# import logging
import itertools
import datetime

from mpi4py import MPI

def log_string(message):
    return "[%s (Node (%s)] %s" % (datetime.datetime.now(), MPI.COMM_WORLD.Get_rank(), message,)

def logger_for(module):
    logger = logging.getLogger(module)
    logger.setLevel(logging.DEBUG)
    con = logging.StreamHandler()
    con.setLevel(logging.DEBUG)
    con.setFormatter(logging.Formatter(
        "%(asctime)s - " + str(MPI.COMM_WORLD.Get_rank()) + " %(name)s - %(levelname)s - %(message)s"
    ))
    logger.addHandler(con)
    return logger

def date_from(filename):
    return filename.split("/")[-1].split(".")[0]

def user_from(url):
    pieces = url.split("/")
    if len(pieces) < 4:
        return pieces[-1]
    else:
        return pieces[3]

def users_in(text):
    tokens = text.split(" ")
    possible_users = filter(lambda x: len(x) > 0 and x[0] == "@", tokens)
    actual_users = []
    for user in possible_users:
        real_name = ""
        for char in user:
            if ord(char) > 47: # and char != ":":
                real_name += char
            elif char == "@":
                real_name += char
            else:
                break
        actual_users.append(real_name[1:])
    return actual_users

def all_files_from(path):
    if path.__CLASS__ is list:
        path = os.join(path)
    return glob.glob(path + "*.db")

def is_supervisor():
    return MPI.COMM_WORLD.Get_rank() == 0

def rank():
    return MPI.COMM_WORLD.Get_rank()

def distribute(data, function):
    comm = MPI.COMM_WORLD
    pass_data = [[]]
    counter = 0
    chunk_size = len(data) / comm.size
    data.reverse()
    pd_index = 0
    if len(data) > comm.size:
        while len(data):
            if counter >= chunk_size and pd_index <= comm.size - 2:
                pass_data.append([])
                pd_index += 1
                counter = 0
            counter += 1
            pass_data[pd_index].append(data.pop())
    else:
        pass_data = map(lambda x: [x], data)
    return comm.gather(
        map(function, 
            comm.scatter(pass_data)
        ), root=0
    )

def flatten(data):
    result = []
    counter = 0
    for item in data:
        for item2 in item:
            result.append(item2)
        counter += 1
    return result

def apply_to_many(fn):
    def do(data):
        return map(fn, data)
    return do

def tagged_with_date(fn):
    def do(date):
        return {
            "date" : date_from(date),
            "data" : fn(date),
        }
    return do

def tags_from(text):
    """
        Extract tags from a text string.
    """
    tags = []
    for line in filter(lambda x: x != "" and x[0] == "#", text.split(" ")):
        string = ""
        for letter in line:
            if ord(letter) < 32:
                break
            string += letter        
        tags.append(string.lower())
    return tags


def write_edgelist(graph, filename):
    file = open(filename, "w")
    for edge in edge_list:
        file.writeline("%s,%s,%s", edge)
    file.close()

def write_weight_matrix(graph, filename):
    # use nodes as headers
    file = csv.DictWriter(open(filename, "w"), graph.nodes)
    # iterate over all nodes
    #   iterate over all a node's edges and convert these to dictionaries
    pass
