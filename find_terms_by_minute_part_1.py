#!/usr/bin/python
# encoding=utf-8
""" Find Takako's search terms minute by minute. 

    This is only half the job; the other is in another file, because
    it goes into 1:00 AM the next day
"""

#!/usr/bin/python
# coding=utf8
import sqlite3

from mpi4py import MPI

from utility import *
import project

# distribute terms to nodes

def find_terms(day):
    day = day[0]
    db_file = project.DATASET_BASE + "/" + day
    input_db = sqlite3.connect(db_file)
    input_db.text_factory = str
    cursor = input_db.cursor()
    # results = {buffer("社長") : {}, buffer("副社長") : {}}
    results = {"社長" : {}, "副社長" : {}}
    terms = ["社長" , "副社長",]
    for term in terms:
        # term = [buffer("%%"), buffer(term), buffer("%%"),]
        # term = "".join(term)
        buf_term = buffer("%%" + term + "%%")
        for row in cursor.execute("""
            SELECT strftime('%H:%M', date) AS minute,
                COUNT(*) AS total FROM tweets
                WHERE body LIKE ?
                GROUP BY strftime('%H:%M', date)
            """, (buf_term,
                # buffer("%%") + 
                # buffer(term) + 
                #    buffer("%%"),)
                    )):
            results[term][row[0]] = row[1]
    input_db.close()
    data = [{
        "time" : "time",
        "社長" : "社長",
        "副社長" : "副社長",
    }]
    for hour in range(24):
        # hour += 14 # because we really want to start at 14:00 hours
        for minute in range(60):
            time = "%s:%s" % (hour, minute,)
            data.append({
                "time" : time,
                "社長" : results["社長"][time] if time in results["社長"] else 0.0,
                "副社長" : results["副社長"][time] if time in results["副社長"] else 0.0,
            })
    out_file = project.open_result_file("terms-by-minute/%s.csv" % date_from(day), "w")(["time", "社長", "副社長",])
    out_file.writerows(data)
    return data
            
if is_supervisor():
    project.make_temp_dir()

comm = MPI.COMM_WORLD
days = map(lambda x: "201103%s.db" % x, [12, 13, 14, 15, 17, 18, 19, 20, 23, 24, 29, 30,])
day = comm.scatter(days),
results = comm.gather(
    find_terms(day),
    root = 0
    )
# results = distribute(terms, find_term)
"""
if is_supervisor():
    data = {}
    # print results
    # results = flatten(results)
    for result in results:
        data[result["term"]] = result["results"]
    headers = ["time",] + terms
    # column_headers = dict(map(lambda h: (h,h,), headers))
    column_headers = 
    rows = [column_headers,]
    for hour in range(24):
        for minute in range(60):
            key = "%02d:%02d" % (hour, minute,)
            row = { "time" : key, }
            for term in terms:
                row[term] = data[term][key] if key in data[term] else 0 
            rows.append(row)
    out_file = project.open_result_file("term-searches/2011-03-14.csv")(headers)
    out_file.writerows(rows)
"""
