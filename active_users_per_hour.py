# active_users_per_hour

def active_users_per_hour(filename):
	file = sqlite3.connect(filename)
	cursor = file.cursor()
	query = """ SELECT strftime("%H:00:00", tweeted_at) AS hour, 
			username, COUNT(*) AS total FROM tweets 
			GROUP BY strftime("%H:00:00", tweeted_at), username 
			HAVING COUNT(*) > 1
			"""
	output_file = project.open_result_file("tweet-lifespans-" + date + ".db")
	output_cursor = output_file.cursor()
	input_query = """ INSERT INTO users (time, username, total_tweets) VALUES (%s,%s,%s) """
	for row in cursor.execute(query):
		output_cursor.execute(input_query, (row["hour"], row["username"], row["total"],))
	output_cursor.commit()
	output_file.close()

def processor(output_file_pattern, row_handler, data_query):
	def process(filename):
		file = sqlite3.connect(filename)
		cursor = file.cursor()
		date = date_from(filename)
		output_file = project.open_result_file(output_file_pattern(date))
		output_cursor = output_file.cursor()
		map(row_handler(output_cursor), cursor.execute(data_query))
		output_cursor.commit()
		output_file.close()
	return process

def active_users_per_hour(filename):
	def row_handler(output_cursor):
		def executor():
			output_cursor.execute(input_query, (row["hour"], row["username"], row["total"],))
		return executor
	def output_filename(date):
		return "tweet-lifespans-" + date + ".db"
	runner = processor(lambda date: "tweet-lifespans-" + date + ".db", row_handler)
	distribute(project.DATASET, runner)