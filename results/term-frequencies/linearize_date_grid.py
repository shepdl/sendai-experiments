import glob
import csv

data = {}
terms = ["day"]
for file in glob.glob("raw-data/*.csv"):
    termname = file.split(".")[0]
    terms.append(termname)
    for line in csv.DictReader(open(file)):
        keys = list(line.keys())
        day = line["day"][0:4] + "-" + line["day"][4:6] + "-" + line["day"][6:]
        for key in keys:
            if key == "day":
                continue
            time = day + " " + key
            data.setdefault(termname, {})
            data[termname][day + " " + key] = line[key]

output_data = []
# iterate over keys (timestamps) in any of the terms
for day in data["TEPCO"]:
    line = dict(map(lambda term: (term, data[term][day],), data))
    line["time"] = day
    output_data.append(line) 
        
output_data.sort(key=lambda k:k["time"])

out = csv.DictWriter(open("term_totals.csv", "w"), list(["time",] + list(data.keys())))
out.writeheader()
out.writerows(output_data)

