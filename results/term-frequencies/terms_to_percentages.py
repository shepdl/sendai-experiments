import csv

# load hourly totals
hourly_totals = {}

for line in csv.DictReader(open("../hourly-tweet-totals.csv")):
    for key in line:
        day = line["day"][0:4] + "-" + line["day"][4:6] + "-" + line["day"][6:8]
        if key != "day":
            hourly_totals[day + " " + key] = float(line[key])

percentages = []
# load data
for line in csv.DictReader(open("term_totals.csv")):
    terms = filter(lambda k: k != "time", line.keys())
    print(line.keys())
    data = {"time" : line["time"],}
    for term in terms:
        data[term] = (int(line[term]) if line[term] != '' else 0) / hourly_totals[line["time"]]
    # percentages.append(dict(map(lambda k: 
    #     (k, (int(line[k]) if line[k] != '' else 0) / hourly_totals[line["time"]],),
    #     terms
    # )))
    percentages.append(data)
    terms = line.keys()

print(percentages)
print(terms)
output = csv.DictWriter(open("term_percentages.csv", "w"), terms )
output.writeheader()
output.writerows(percentages)
