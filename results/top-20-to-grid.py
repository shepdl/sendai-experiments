import csv

# grab all rows
# convert to format:
# date,tag1,tag2
# {day},count,count ...
data = {}
tag_headers = ["day",]
for row in csv.reader(open("hashtag-top-20.csv")):
    data.setdefault(row[0], {})
    data[row[0]][row[1]] = row[2]
    tag_headers.append(row[1])
    
tag_headers = list(set(tag_headers))

out_data = []
out_data.append(dict(map(lambda x: (x,x,), tag_headers)))

for day in data:
    row = data[day]
    row["day"] = day
    print row
    out_data.append(row)

out = csv.DictWriter(open("hashtag-top-20-grid.csv", "w"), tag_headers)
out.writerows(out_data)
