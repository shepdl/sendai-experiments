#!/usr/bin/python
# coding=utf8
import sqlite3

from utility import *
import project

def word_count(filename):
    word_counts = {}
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()
    last_time = None
    out = project.open_result_file(
        "word-frequencies-" + date_from(filename) + ".csv"
    )(["hour", "word", "count",])

    for row in cursor.execute("""
        SELECT strftime('%H', date), segmented_body, count(*)
        FROM tweets
        GROUP BY body_hash
    """):
        when, body, count = row
        if last_time is None:
            last_time = when
        if last_time != when:
            to_print = []
            for word, count in word_counts.iteritems():
                to_print.append({
                    "hour": when, "word" : word, "count" : count,
                })
            to_print = sorted(to_print, key=lambda word: word["count"])
            out.writerows(to_print)
            to_print = []
            word_counts = {}
        # Make sure that the entire word is Japanese
        # no Roman characters, numbers, or punctuation
        # NOTE: this looks counterintuitive, because we
        # are testing against string.printable, which is
        # the union of those sets
        words = filter(lambda word: all(
                c not in string.printable for c in word
            ),
            body.split()
        )
        for word in words:
            word_counts.setdefault(word, 0)
            word_counts[word] += 1

    cursor.close()
    conn.close()
    to_print = []
    for word, count in word_counts.iteritems():
        to_print.append({
            "hour" : when, "word" : word, "count" : count,
        })
    to_print = sorted(to_print, key=lambda word: word["count"])
    out = project.open_result_file("word-frequencies-" + date_from + ".csv")(["word", "count",])
    out.writerows(to_print)
    out.close()

files = distribute(project.DATASET_SEGMENTED, word_count)
