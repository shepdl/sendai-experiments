DISPATCHER_NODE_ID = 0
# NOTE: range() returns a list of ints UP TO the last number
# range() is non-inclusive.
COUNTER_NODE_IDS = range(1,5)
AGGREGATOR_NODE_IDS = range(5,9)

