#!/usr/bin/python
# coding: utf-8
# distribute by day

import sqlite3

import project
from utility import *

from words_to_track import WORDS_TO_TRACK
WTS_START = 0
WTS_END = 5000

# load all tweets
def discover_word_times(filename):
    # iterate through each tweet; see if it uses any of the words
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()
    WTS = {}
    for word in WORDS_TO_TRACK[WTS_START:WTS_END]:
        if len(word) > 0:
            WTS.setdefault(word[0], [])
            WTS[word[0]].append(word)
    # WTS = WORDS_TO_TRACK[0:10000]
    word_positions = dict([(word, WORDS_TO_TRACK.index(word),) for word in WORDS_TO_TRACK])
    out_file = codecs.open("%s/sendai-output/word-bursts/%s.csv" % (
        os.environ['SCRATCH'], date_from(filename),), 
        "w", encoding="utf-8-sig"
    )
    word_timestamps = dict([(word, [],) for word in WORDS_TO_TRACK[WTS_START:WTS_END] if len(word) > 0])
    for row in cursor.execute("""
            SELECT date, segmented_body FROM tweets
        """):
        tweeted_at, words = row
        if not words:
            continue
        # if so, add that time in the word column
        words_to_update = [word for word in words.split() if len(word) > 0 and word[0] in WTS and word in WTS[word[0]]]
        for word in words_to_update:
            word_timestamps[word].append(tweeted_at)
    return word_timestamps

day_reports = distribute(project.DATASET_SEGMENTED, discover_word_times)

if is_supervisor():
    print "Compiled word counts; in supervisor now ..."
    data = flatten(day_reports)
    master_reports = {}
    for result_dict in data:
        for word, dates in result_dict.iteritems():
            master_reports.setdefault(word, [])
            master_reports[word] += dates

    for word, data in master_reports.iteritems():
        out_file = project.open_result_file("word_times/%s.csv" % (word,))
        out_file.write("\n".join(sorted(data)))
        out_file.close()
    
# export a CSV file where the headers are words and each column is a series in time
# or, write a separate file for each word
# to do this, reduce matrix at the end
