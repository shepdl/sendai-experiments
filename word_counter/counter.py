# -*- coding: utf-8 -*- 
# counter.py: counter program for word_count

from config import *
from node import Node
from mpi4py import MPI
COMM = MPI.COMM_WORLD

JAPANESE_UNICODE_START = 12352 #12288 is the official start, but that includes punctuation
JAPANESE_UNICODE_END = 19888

AGGREGATOR_DISPATCH_TABLE = dict(map(
    lambda i: (i + AGGREGATOR_NODE_IDS[0], (
        (i * ((JAPANESE_UNICODE_END - JAPANESE_UNICODE_START) / len(AGGREGATOR_NODE_IDS)) + JAPANESE_UNICODE_START),
        ((i + 1) * ((JAPANESE_UNICODE_END - JAPANESE_UNICODE_START) / len(AGGREGATOR_NODE_IDS)) + JAPANESE_UNICODE_START) - 1,
    ),),
    range(len(AGGREGATOR_NODE_IDS))
))

STOPWORDS = [u"これ", u"それ", u"あれ", u"この", u"その", u"あの", u"ここ", u"そこ", u"あそこ", 
    u"こちら", u"どこ", u"だれ", u"なに", u"なん", u"何", u"私", u"貴方", u"貴方方", u"我々", u"私達", u"あの人", 
    u"あのかた", u"彼女", u"彼", u"です", u"あります", u"おります", u"います", u"は", u"が", u"の", u"に", u"を", 
    u"で", u"え", u"から", u"まで", u"より", u"も", u"どの", u"と", u"し", u"それで", u"しかし",
]

class Counter(Node):

    def __init__(self):
        super(Counter, self).__init__("Counter")
        self.setup_dispatch_table({
            "count_words" : self.count_words,
        })

    def count_words(self, tweets):
        self.log("Received tweets ...")
        words = {}
        for tweet in tweets:
            # valid_words = filter(lambda word: word[0] not in string.printable and word not in STOPWORDS, tweet.split())
            valid_words = filter(lambda word: ord(word[0]) >= JAPANESE_UNICODE_START and ord(word[0]) <= JAPANESE_UNICODE_END and word not in STOPWORDS, tweet.split())
            for word in valid_words:
                words.setdefault(word, 0)
                words[word] += 1
        self.send_aggregators(words)
        COMM.send({
            "name" : "counter_finished",
            "data" : {
                "counter_id" : COMM.Get_rank(),
            }
        }, dest=DISPATCHER_NODE_ID)

    def send_aggregators(self, words):
        self.log("Sending %s words to aggregators." % (len(words),))
        grouped_by_nodes = dict(map(lambda i: (i, [],), AGGREGATOR_NODE_IDS))
        for word, count in words.iteritems():
            grouped_by_nodes[ (ord(word[0]) % len(AGGREGATOR_NODE_IDS)) + AGGREGATOR_NODE_IDS[0]].append((word, count,))
            # for i, (start, end) in AGGREGATOR_DISPATCH_TABLE.iteritems():
            #     self.log(ord(word[0]))
            #     if ord(word[0]) >= start and ord(word[0]) <= end:
            #        grouped_by_nodes[i].append((word, count,))
            #        break

        for node, words in grouped_by_nodes.iteritems():
            COMM.send({
                "name" : "new_words", 
                "data" : {
                    "word_list" : words,
                },
            }, dest=node)

