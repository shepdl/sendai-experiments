#!/usr/bin/python
# calculate_frequency_distributions.py
import datetime
import os
import glob
import csv
import codecs

import project
from utility import *


# distribute among nodes
def calculate_hourly_distribution(date):
    hour = date.strftime("%H")
    if hour[0] == "0":
        hour = hour[1]
    filename_date = date.strftime("%Y%m%d-") + hour + "-00"
    filename_pattern = "%s/sendai-output/word_counts/%s-*.csv"  % (
        os.environ["SCRATCH"], filename_date,
    )
    total_words = 0
    distribution = {}
    for in_filename in glob.glob(filename_pattern):
        in_file = codecs.open(in_filename, encoding="utf8")
        # skip headers
        in_file.readline()
        print log_string("Loading %s" % (in_filename,))
        for line in in_file:
            row = line.split(",")
            # Reduce differences in frequencies to about 0.001 percent
            value = float(row[-1]) 
            distribution.setdefault(value, 0)
            total_words += value
            distribution[value] += 1
    # expect date to look like '20110309 08:00'
    timestamp = date.strftime("%Y-%m-%d %H:00")
    # date = (" ".join(filename_pattern.split("/")[-1].split("-")[0:2])) + ":00"

    frequency_distributions = {}
    for key, value in distribution.iteritems():
        # group values that are close, based on floating-point groups
        new_key = "%0.3f" % ((key / total_words) * 100,)
        frequency_distributions.setdefault(new_key, 0)
        frequency_distributions[new_key] += value

    # divide by total number of possible values to normalize distribution of
    # distribution
    total_possible_values = float(sum(frequency_distributions.itervalues()))

    # set this here because we need to sum this above
    normalized_values = {  
        "timestamp" : timestamp,
    }
    for key, value in frequency_distributions.iteritems():
        normalized_values[key] = float("%0.3f" % ( value / total_possible_values * 100,))
    
    return normalized_values

if __name__ == "__main__":
    # create a list of all files based on date
    hour_files = [datetime.datetime(year=2011, month=3, day=9, hour=0) + 
                    datetime.timedelta(hours=h)
        for h in range(25 * 24)
    ]

    all_reports = distribute(hour_files, calculate_hourly_distribution)

    if is_supervisor():
        # expect pattern {"timestamp" : date, "dist1" : val1, "dist2" : val2.}
        all_reports = flatten(all_reports)
        frequencies_by_hour = {}
        for values in all_reports:
            # frequencies_by_hour.setdefault(values["timestamp"], {})
            for frequency, count in values.iteritems():
                if frequency == "timestamp":
                    continue
                frequencies_by_hour.setdefault(frequency, {})
                frequencies_by_hour[frequency].setdefault(values["timestamp"], 0)
                #frequencies_by_hour[values["timestamp"]].setdefault(frequency, 0)
                frequencies_by_hour[frequency][values["timestamp"]] += count
        headers = ["frequency",] + [
            (datetime.datetime(year=2011, month=3, day=9, hour=0) + 
                datetime.timedelta(hours=h)).strftime("%Y-%m-%d %H:00")
            for h in range(25 * 24)
        ]
        print frequencies_by_hour[frequencies_by_hour.keys()[100]]
        frequency_list = []
        out_file = project.open_result_file("word_frequency_distributions_4sd.csv")(headers)
        out_file.writerow(dict([(h,h,) for h in headers]))
        for frequency in sorted(frequencies_by_hour.keys()):
            values = frequencies_by_hour[frequency]
            v = {"frequency" : frequency,}
            v.update(values)
            out_file.writerow(v)
        
