import sys

import sqlite3

from mpi4py import MPI
COMM = MPI.COMM_WORLD

from utility import date_from

from node import Node
from config import *

class Dispatcher(Node):

    def __init__(self):
        super(Dispatcher, self).__init__("Dispatcher")
        self.database_filename = sys.argv[1] # database_filename
        self.log("Using file %s" % (self.database_filename,))
        self.database_connection = sqlite3.connect(self.database_filename)
        self.set_index = 0
        self.SET_WIDTH = 10000
        self.hour = 0
        self.finished_counters = []

        self.setup_dispatch_table({
            "counter_finished" : self.counter_finished,
            "summarize_finished" : self.summarize_finished,
            "exception" : self.handle_exception,
        })

    def get_tweets(self):
        tweets = []
        cursor = self.database_connection.cursor()
        self.log("Loading new tweets")
        for row in cursor.execute("""SELECT segmented_body FROM tweets 
                WHERE strftime('%%H', date) = '%02d'
                LIMIT ?,?
            """ % (self.hour,), (self.set_index * self.SET_WIDTH, 
                self.SET_WIDTH,)
            ):
            tweets.append(row[0])
        self.set_index += 1
        cursor.close()
        return tweets

    def run(self):
        while self.listening:
            self.log("Starting dispatch process ...")
            for aggregator in AGGREGATOR_NODE_IDS:
                # self.log("Cleaning aggregator %s." % (aggregator,))
                COMM.send({
                    "name" : "flush",
                    "data" : {},
                }, dest=aggregator)
            for counter in COUNTER_NODE_IDS:
                COMM.send({
                    "name" : "count_words",
                    "data" : {
                        "tweets" : self.get_tweets(),
                    }
                }, dest=counter) 
            super(Dispatcher, self).run()

    def counter_finished(self, counter_id=0):
        self.log("Counter finished.")
        tweets = self.get_tweets()
        if len(tweets) > 0:
            COMM.send({
                "name" : "count_words",
                "data" : {
                    "tweets" : tweets,
                },
            }, dest=counter_id)
            self.log("New tweets sent.")

        else:
            self.finished_counters.append(counter_id)
            if len(self.finished_counters) == len(COUNTER_NODE_IDS):
                self.summarize()

    def summarize(self):
        self.log("Asking for summary reports.")
        for node in AGGREGATOR_NODE_IDS:
            COMM.send({
                "name" : "summarize", 
                "data" : {
                    "filename_base" : ("word_counts/%s-%s-00" % (
                        date_from(self.database_filename), self.hour,
                    )) + "-%s.csv",
                }
            }, dest=node)
        self.database_connection.close()

    def summarize_finished(self, node_id=0):
        self.log( "Finished summarize for node %s ..." % (node_id,))
        self.finished_aggregators.append(node_id)
        if len(self.finished_aggregators) == len(AGGREGATOR_NODE_IDS):
            self.hour += 1
            self.log("Finished %s:00; starting %s:00" % (self.hour - 1, self.hour,))
            if self.hour > 24:
                self.listening = False
            # otherwise, fall off the end ...

    def handle_exception(exception):
        # shut down all nodes
        # write log
        pass

class API(object):
    def summary_finished(self, results):
        # send message
        message = {
            "sender" : COMM.Get_rank(),
            "results" : results,
        }
        COMM.send(message, dest=DISPATCHER_NODE_RANK)
