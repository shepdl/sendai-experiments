import codecs
import string
import json as json

from words_to_track import WORDS_TO_TRACK

stopwords_file = codecs.open("stopwords.txt", encoding="utf-8")

stopwords = []
for line in stopwords_file:
    stopwords.append(line.strip())

out_file = codecs.open("filtered_words.py", "w", encoding="utf-8-sig")
new_words = []
for word in WORDS_TO_TRACK:
    try:
        int(word)
    except ValueError:
        if len([char for char in word if char in string.punctuation]) == 0:
            if word not in stopwords:
                new_words.append(word)
json.dump(new_words, out_file, indent=4)
out_file.close()
