#!/usr/bin/python

import os
import glob
import re
import codecs

import numpy as np
from scipy.cluster.vq import kmeans2

# compile all users
user_percentages = {}
for in_file in glob.glob("%s/sendai-output/user_ranks/*.csv" % (
    os.environ['SCRATCH'],)):
    in_file = codecs.open(in_file, encoding="utf-8")
    # count value distributions: among users who sent more than five messages, 
    in_file.readline()
    for line in in_file:
        line = re.sub(r"\[[\d., ]*],", "", line)
        username, on_topic_count, off_topic_count, percent = line.split(",")
        on_topic_count = int(on_topic_count)
        off_topic_count = int(off_topic_count)
        percent = float(percent)
        if on_topic_count + off_topic_count >= 10:
            user_percentages.setdefault(username, [])
            user_percentages[username].append(percent)            
    # what were the breakdowns like?
    # count users who have the same values, or at least fall into the same percentages
    # do k-menas clustering

users = user_percentages.keys()
percentages = np.zeros(len(user_percentages))
counter = 0
for user in users:
    # users[user] = sum(percentages) / len(percentages)
    percentages[counter] = sum(user_percentages[user]) / len(user_percentages[user])
    counter += 1
    # users.append((user, sum(percentages) / len(percentages),))

clusters, labels = kmeans2(percentages, 6)
# divided_clusters = np.array((6, len(labels) / 2,))
divided_clusters = {}
for cluster, user in zip(labels, users):
    divided_clusters.setdefault(cluster, [])
    divided_clusters[cluster].append(user)

# for cluster in clusters:
#     divided_clusters[cluster] = [
#         user for (user, label) in zip(users, labels.tolist())
#             if label == cluster
#     ]
# output: list of users broken down by clusters
# format:
# Cluster 1: (centroid) Total users: (users)
#     ... list of users ...
# TODO: is there an automatic way to partition an np.array based on users?
out_file = codecs.open("../results/user_disaster_clusters.report.txt", "w", encoding="utf-8-sig")
clusters = [str(x) for x in clusters]
out_file.write( "%s\n" % ("".join(clusters),))
for cluster, users in divided_clusters.iteritems():
    out_file.write(""" 
    Cluster %s with %s users:

    %s \n""" % (
        cluster, len(users),
        "\n".join(users),
    ))

out_file.close()
