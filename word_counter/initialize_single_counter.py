#!/usr/bin/python
from mpi4py import MPI
COMM = MPI.COMM_WORLD

from communication import for_rank, run
from dispatcher_counter import Dispatcher
# from counter import Counter
from aggregator import Aggregator
from config_single_counter import *


if __name__ == "__main__":
    this_node = run([
        for_rank(DISPATCHER_NODE_ID).assign_role_of(Dispatcher),
        for_rank(ids=AGGREGATOR_NODE_IDS).assign_role_of(Aggregator),
    ])
    if COMM.Get_rank() == DISPATCHER_NODE_ID:
        # this_node.start()
        pass
