# -*- coding: utf-8 -*- 
import codecs
import os

from mpi4py import MPI
import project

COMM = MPI.COMM_WORLD

from node import Node

# Dispatcher is closest lower node where node is evenly divisible by 5
DISPATCHER_NODE_ID = COMM.Get_rank() / 5 * 5

class Aggregator(Node):

    def __init__(self):
        super(Aggregator, self).__init__("Aggregator")
        self.word_table = {}
        self.setup_dispatch_table({
            "new_words": self.new_words,
            "summarize" : self.summarize,
            "flush" : self.flush,
        })

    def new_words(self, word_list=None):
        self.log("Aggregator received %s words." % (len(word_list),))
        for (word, count) in word_list:
            self.word_table.setdefault(word, 0)
            self.word_table[word] += count

    def summarize(self, filename_base=""):
        # write to file
        # return top 100 words
        self.log("Preparing summary.")
        summary = []
        # out = project.open_result_file(filename_base % (COMM.Get_rank(),), "w"
        #     )(["word", "count",])
        # out.writerow({"word" : "word", "count" : "count",})
        for word, value in self.word_table.iteritems():
            # out.writerow({"word" : unicode(word).encode("utf-8"), "count" : value,})
            summary.append((word, value,))
        summary = sorted(summary, key=lambda x: x[1])
        summary.reverse()
        # out = codecs.open(os.path.join(os.environ["HOME"], "sendai-experiments", "results",
        out = codecs.open(os.path.join(os.environ["SCRATCH"], "sendai-output", 
            filename_base % (COMM.Get_rank(),)), "w", encoding="utf-8-sig")
        out.write("word,count\n")
        for item in summary:
            out.write("%s,%s\n" % item)
        out.close()
        COMM.send({
                "name" : "summarize_finished",
                "data" : {
                    "node_id" : COMM.Get_rank(),
                    "summary" : [], # summary[0:50],
                }
            }, dest=DISPATCHER_NODE_ID)
        self.log("Summary sent.")

    def flush(self):
        self.log("Flushing.")
        self.word_table = {}

