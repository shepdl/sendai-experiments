# count_words.py -- discover how many distinct words exist together

# spawn separate processes for each 100,000 tweets; these rotate through each 100,000 tweets
# and collect word counts
# at the end, they get reduced into one large list for each hour
#   sort the words and then segment the list


# I want to be able to say something like this:
for_rank(1).assign_role_of(Dispatcher)
for_rank(start=1, through=4).assign_role_of(Counter)
#for_rank(start=4, using_function=lambda x: x % 2 == 0).assign_role_of(Aggregator)
# for_rank returns an object only if this is the correct node
# this goes in a separate initialize.py file, or something like that, which is where it all beginsc

def for_rank(start=0, through=None):
    range_to_test = [start]
    if through is not None:
        range_to_test = map(lambda i: start + i, range(i))
    if MPI.Get_rank() in range_to_test:
        return Role()
    else: 
        return WrongRole()

class Role(object):
    def assign_role_of(klass):
        self.klass = klass

    def start():
        self.klass = klass()
        self.klass.run()

class WrongRole(Role):
    def assign_role_of(klass):
        pass


def message_stream():
    messages = MPI.recv(MPI.ANY_RANK)
    if message is list:
        for messages in messages:
            yield message
    else:
        yield message


for message in message_stream():
    dispatch(message)