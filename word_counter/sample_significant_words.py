#!/usr/bin/python
# sample_significant_words.py
import os
import glob
import csv
import codecs
import math
import datetime

import numpy.random

from utility import *
import project

def breakdown_for(max_value):
    # cube_root = scipy.special.cbrt(max_value)
    fourth_root = math.sqrt(math.sqrt(max_value))
    # breakdowns = [(0, cube_root ),] + [(cube_root ** x, cube_root ** (x + 1), ) for x in range(1,3)]
    breakdowns = [(0, fourth_root), ] + [(fourth_root** x, fourth_root** (x + 1), ) for x in range(1, 3)]
    def bd_tester(test):
        counter = 0
        for (left, right,) in breakdowns:
            if test >= left and test <= right:
                return counter
            counter += 1
        return counter
    return bd_tester

# for each hour in each day ...
def sample_significant_words(date):
    # find total tokens in every hour

    hour = date.strftime("%H")
    if hour[0] == "0":
        hour = hour[1]
    filename_date = date.strftime("%Y%m%d-") + hour + "-00"

    filename_pattern = "%s/sendai-output/word_counts/%s-*.csv"  % (
        os.environ["SCRATCH"], filename_date,
    )

    node_files = [
        # csv.DictReader(codecs.open(in_filename)) 
        codecs.open(in_filename)
            for in_filename in glob.glob(filename_pattern)
    ]

    # skip header
    map(lambda f: f.readline(), node_files)

    total_tokens = 0
    for in_file in node_files:
        for line in in_file:
            total_tokens += float(line.split(",")[-1])

    x = """
    total_tokens = sum([
        float(line["count"].split(",")[-1]) for line in [x for x in [f for f in [# for line in node_files # [ 
           csv_file for csv_file in [
               in_file for in_file in node_files
           ]
        ]]]
    ])
    """

    possibly_significant_words = {
        0 : [],
        1 : [],
        2 : [],
        3 : [],
    }
    cutoff = 0.005 * total_tokens
    # TODO: multiply cutoff and count / total_tokens by 1000 so that it's still positive
    breakdown = breakdown_for(cutoff * 1000)
    for csv_file in node_files:
        csv_file.seek(0)
        csv_file.readline()
        for line in csv_file:
            pieces = line.split(",")
            word = pieces[0]
            count = int(pieces[-1])
            if count >= 60 and count <= cutoff: 
                # (count / total_tokens) >= 0.05: # <= 0.002:
                new_count = int(100000 * (float(count) / total_tokens))
                # new_count = 1000 * count
                count = 1000 * count
                if new_count > 0:
                    possibly_significant_words[breakdown(count)].append((word, new_count,))
        # find breakdowns of words that occur less than 0.002% of the time
    # make a spreadsheet or graph of this
    # compute mean of all word frequencies (within the local population of these significant words), then grab about 10% of the words from 1 std dev, 2 std devs, etc., or using a logarithmic scale (find the breakdown point by calculating x ** 3 = y where y = the max distribution (which solves to x = y log 3))
    # print these out to a file
    # translate the words -- what do they mean?
    out_file = codecs.open("%s/sendai-output/word_samples_under_0005_times_100K/%s.csv" % (os.environ["SCRATCH"], date.strftime("%Y-%m-%dT%H:00"), ), "w")
    for value in range(1, 4):
        # numpy.random.shuffle(possibly_significant_words[value])
        # for tup in possibly_significant_words[value][0:25]:
        for tup in possibly_significant_words[value]:
            out_file.write("%s,%s\n" % tup)

hour_files = [datetime.datetime(year=2011, month=3, day=9, hour=0) + 
                datetime.timedelta(hours=h)
    for h in range(25 * 24)
]

all_reports = distribute(hour_files, sample_significant_words)
