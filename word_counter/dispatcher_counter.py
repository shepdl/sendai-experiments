# -*- coding: utf-8 -*- 
import sys
import os
import codecs

import sqlite3

from mpi4py import MPI
COMM = MPI.COMM_WORLD

from utility import date_from

from node import Node
from config_single_counter import *

JAPANESE_UNICODE_START = 12352 #12288 is the official start, but that includes punctuation
JAPANESE_UNICODE_END = 19888

# Each dispatcher has four aggregators, which are the four nodes above it in rank
AGGREGATOR_NODE_IDS = [x + COMM.Get_rank() + 1 for x in range(4)] 

AGGREGATOR_DISPATCH_TABLE = dict(map(
    lambda i: (i + AGGREGATOR_NODE_IDS[0], (
        (i * ((JAPANESE_UNICODE_END - JAPANESE_UNICODE_START) / len(AGGREGATOR_NODE_IDS)) + JAPANESE_UNICODE_START),
        ((i + 1) * ((JAPANESE_UNICODE_END - JAPANESE_UNICODE_START) / len(AGGREGATOR_NODE_IDS)) + JAPANESE_UNICODE_START) - 1,
    ),),
    range(len(AGGREGATOR_NODE_IDS))
))

STOPWORDS = [u"これ", u"それ", u"あれ", u"この", u"その", u"あの", u"ここ", u"そこ", u"あそこ", 
    u"こちら", u"どこ", u"だれ", u"なに", u"なん", u"何", u"私", u"貴方", u"貴方方", u"我々", u"私達", u"あの人", 
    u"あのかた", u"彼女", u"彼", u"です", u"あります", u"おります", u"います", u"は", u"が", u"の", u"に", u"を", 
    u"で", u"え", u"から", u"まで", u"より", u"も", u"どの", u"と", u"し", u"それで", u"しかし",
]

NODE = (["201103%02d" % (x + 9,) for x in range(23)] + ["20110401", "20110402",])[COMM.Get_rank() / 5]

stopwords_file = codecs.open("stopwords.txt", encoding="utf-8")
for line in stopwords_file:
    STOPWORDS.append(line.strip())

stopwords_file.close()

class Dispatcher(Node):

    def __init__(self):
        super(Dispatcher, self).__init__("Dispatcher")
        self.database_filename = os.path.join(os.environ["SCRATCH"], "sendai-segmented", "%s.db" % (NODE,) )
        self.log("Using file %s" % (self.database_filename,))
        self.database_connection = sqlite3.connect(self.database_filename)
        self.set_index = 0
        self.SET_WIDTH = 10000
        self.hour = 0
        self.finished_aggregators = []

        self.setup_dispatch_table({
            "summarize_finished" : self.summarize_finished,
        })

    # dispatcher 2:
    def run(self):
        cursor = self.database_connection.cursor()
        for hour in range(24):
            self.hour = hour
            for aggregator in AGGREGATOR_NODE_IDS:
                self.log("Cleaning aggregator %s." % (aggregator,))
                COMM.send({
                    "name" : "flush",
                    "data" : {},
                }, dest=aggregator)
            tweets = []
            self.log("Loading tweets")
            counter = 0
            for row in cursor.execute("""SELECT segmented_body FROM tweets 
                    WHERE strftime('%%H', date) = '%02d' """ % (hour,), ):
                tweets.append(row[0])
                counter += 1
                if counter == 10000:
                    self.count_words(tweets)
                    tweets = []
                    counter = 0
            # Pick up any last tweets
            self.count_words(tweets)
            self.summarize()
            self.log("Listening for summary_finished messages ...")
            super(Dispatcher, self).run()
            self.log("Finished %s:00; starting %s:00" % (hour, hour + 1,))
        # Clean up
        for aggregator in AGGREGATOR_NODE_IDS:
            self.log("Shutting down aggregator %s ..." % (aggregator,))
            COMM.send({
                "name" : "stop",
                "data" : {},
            }, dest=aggregator)
        cursor.close()
        self.database_connection.close()

    def count_words(self, tweets):
        self.log("Received tweets ...")
        words = {}
        for tweet in tweets:
            if not tweet:
                continue
            valid_words = filter(
                # lambda word: ord(word[0]) >= JAPANESE_UNICODE_START and ord(word[0]) <= JAPANESE_UNICODE_END and word not in STOPWORDS, 
                lambda word: word not in STOPWORDS, 
                tweet.split()
            )
            for word in valid_words:
                words.setdefault(word.lower(), 0)
                words[word.lower()] += 1
        self.send_aggregators(words)

    def send_aggregators(self, words):
        self.log("Sending %s words to aggregators." % (len(words),))
        grouped_by_nodes = dict(map(lambda i: (i, [],), AGGREGATOR_NODE_IDS))
        for word, count in words.iteritems():
            grouped_by_nodes[ (ord(word[0]) % len(AGGREGATOR_NODE_IDS)) + AGGREGATOR_NODE_IDS[0]].append((word, count,))
            # for i, (start, end) in AGGREGATOR_DISPATCH_TABLE.iteritems():
            #     self.log(ord(word[0]))
            #     if ord(word[0]) >= start and ord(word[0]) <= end:
            #        grouped_by_nodes[i].append((word, count,))
            #        break
        for node, words in grouped_by_nodes.iteritems():
            COMM.send({
                "name" : "new_words", 
                "data" : {
                    "word_list" : words,
                },
            }, dest=node)

    def summarize(self):
        self.log("Asking for summary reports.")
        self.summary = []
        for node in AGGREGATOR_NODE_IDS:
            COMM.send({
                "name" : "summarize", 
                "data" : {
                    "filename_base" : ("word_counts/%s-%s-00" % (
                        date_from(self.database_filename), self.hour,
                    )) + "-%s.csv",
                }
            }, dest=node)

    def summarize_finished(self, node_id, summary):
        self.log( "Finished summarize for node %s ..." % (node_id,))
        # self.summary.append(summary)
        self.finished_aggregators.append(node_id)
        if len(self.finished_aggregators) == 4: #len(AGGREGATOR_NODE_IDS):
            self.log("All summaries finished.")
            self.listening = False
            self.finished_aggregators = []
            # summary = sorted(self.summary, key=lambda c: c[1])
            return self.STOP_LISTENING

