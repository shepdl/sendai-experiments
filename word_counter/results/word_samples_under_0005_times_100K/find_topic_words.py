import codecs

in_file = codecs.open("headers_from_over_100K.csv", encoding="utf-8")

words = []
in_file.readline()
for line in in_file:
    pieces = line.split("\t")
    if len(pieces) >= 4 and pieces[3] == u"\"disaster\"":
        words.append(pieces[1][1:-1])

out_file = codecs.open("disaster_words.txt", "w", encoding="utf-8-sig")
out_file.write("[")
for line in words:
    out_file.write("'%s'," % (line,))
out_file.write("]")
