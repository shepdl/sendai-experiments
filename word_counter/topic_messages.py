#!/usr/bin/python
# coding=utf8

# rate user's tweets
import os
import codecs
import sqlite3

import project
from utility import *

TOPIC_WORDS = [u'つながら',u'明大',u'ポイント',u'器',u'行動',u'配布',u'盲導犬',u'回し',u'恐れ',u'など',u'注意報',u'プレート',u'ぜひ',u'済ませ',u'国内',u'方',u'透析',u'繋がら',u'繋がり',u'通話',u'場所',u'津波',u'動け',u'動か',u'動い',u'発生',u'国連',u'鎌倉',u'重油',u'医療',u'速報',u'向陽台',u'治安',u'国道',u'アクセス',u'ひ',u'献金',u'割れ',u'繰り返し',u'救出',u'死亡',u'実家',u'バス',u'とる',u'体育館',u'料',u'落ち着い',u'妊婦',u'広告',u'歩い',u'気象庁',u'保安',u'塩',u'保険',u'設備',u'無料',u'社内',u'施設',u'管',u'パーク',u'千葉製油所',u'原発',u'救急',u'福島',u'社員',u'身',u'自転車',u'降っ',u'救援',u'近辺',u'行方',u'チェルノブイリ',u'ン',u'行政',u'九州',u'見れる',u'症',u'皆',u'生き埋め',u'階',u'願う',u'学校',u'皆様',u'sos',u'品',u'爆発',u'倒壊',u'障害',u'餓死',u'男性',u'発令',u'セシウム',u'東京',u'県内',u'ガス',u'お祈り',u'人災',u'太平洋',u'ユニセフ',u'政見',u'赤十字',u'本当',u'生放送',u'首相',u'揮発',u'少ない',u'混雑',u'食料',u'詐欺',u'赤',u'健康',u'病院',u'食品',u'近づか',u'祈り',u'疎開',u'グリコ',u'nuclear',u'ニコ',u'怖かっ',u'余震',u'焦る',u'生存',u'消防',u'日常',u'混乱',u'総理',u'水力',u'韓国',u'さいたま',u'tsunami',u'弟',u'シーベルト',u'受け入れ',u'祈る',u'高速',u'半蔵門線',u'都営',u'犯罪',u'避け',u'日本赤十字社',u'helpme',u'被災',u'メルトダウン',u'被爆',u'非常',u'漏れ',u'都立',u'住宅',u'天気',u'earthquake',u'不謹慎',u'女',u'復興',u'どなた',u'tepco',u'出血',u'防災',u'漏電',u'誕生',u'人命',u'疲れ',u'社長',u'マイクロシーベルト',u'天罰',u'医学',u'震災',u'fukushima',u'線量',u'震度',u'亡くなら',u'海岸',u'衝撃',u'亡くなっ',u'センター',u'痛い',u'リスク',u'中部電力',u'節電',u'地震',u'消防庁',u'死ぬ',u'ミリシーベルト',u'prayforjapan',u'反応',]

# get all messages; iterate through them
def rank_tweets_by_topic(filename):
    users = {}
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()
    for row in cursor.execute(""" SELECT username, segmented_body FROM tweets """):
        username, body = row
        # rank each message based on how many words it uses from the dictionary
        # assign score to hashmap of user -> [messages]
        users.setdefault(username, [])
        if body is None:
            body = ""
        score = sum([1 for word in body.split() if word in TOPIC_WORDS])
        if score > 1:
            users[username].append(1)
        else:
            users[username].append(0)
        # rank user at the end of the day for how many messages they sent that had to do with these topics
    out_filename = "%s/sendai-output/user_ranks/%s.csv" % (os.environ['SCRATCH'], date_from(filename),)
    out_file = codecs.open(out_filename, "w", encoding="utf-8-sig")
    out_file.write("username,topic messages,ranks,non-topic messages,percent on-topic\n")
    total_topic_messages = 0
    total_nontopic_messages = 0
    for username, messages in users.iteritems():
        topic_messages = sum(messages)
        nontopic_messages = len(messages) - sum(messages)
        percent_on_topic = float(topic_messages) / len(messages)
        out_file.write("%s,%s,%s,%s,%s\n" % (
            username, topic_messages, messages, nontopic_messages, percent_on_topic,
        ))
        total_topic_messages += topic_messages
        total_nontopic_messages += nontopic_messages
    out_file.write("%s,%s,%s,%s\n" % ("Total", 
        total_topic_messages, total_nontopic_messages, float(total_topic_messages) / total_nontopic_messages,)
    )
    out_file.close()

distribute(project.DATASET_SEGMENTED, rank_tweets_by_topic)
