import codecs
import glob

counts = {}
for csv_file in glob.glob("sendai-experiments/results/word_counts/*.csv"):
    print csv_file
    stream = codecs.open(csv_file, encoding="utf-8")
    stream.readline() # skip header
    counter = 0
    for line in stream:
        counter += 1
        word, value = line.rsplit(",", 1)
        value = int(value)
        # if counter >= 500:
        #     break
        counts.setdefault(word, 0)
        counts[word] += value

counts = sorted(counts.iteritems(), key=lambda x: x[1])
counts.reverse()
out = codecs.open("sendai-experiments/results/2011-03-16-top_50_words.csv", "w", encoding="utf-8")
for line in counts:#[0:500]:
    out.write("%s,%s\n" % line)

out.close()
