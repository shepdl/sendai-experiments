#!/usr/bin/python
# coding=utf8
import sqlite3

from utility import *
import project

def find_terms(filename):
    terms = "政府 社員 現場 隠ぺい 臨界 保安 保安院 節電 計画停電 対策 停止 逃げる 消防 自衛隊 注水 枝野 清水 電源 課長 部長 電源"
    terms = terms.split(" ")
    input_db = sqlite3.connect(filename)
    cursor = input_db.cursor()
    term_results = {"day" : date_from(filename),}
    for term in terms:
        term_results[term] = {}
        for row in cursor.execute("""
            SELECT strftime("%H:00:00", date) AS hour,
                COUNT(*) AS total FROM tweets
                WHERE body LIKE ?
                GROUP BY strftime("%H", date)

        """, (buffer("%%"+term+"%%"),)):
            term_results[term][row[0]] = int(row[1])
    input_db.close()
    return term_results

results = distribute(project.DATASET_SEGMENTED, find_terms)
terms = "政府 社員 現場 隠ぺい 臨界 保安 保安院 節電 計画停電 対策 停止 逃げる 消防 自衛隊 注水 枝野 清水 電源 課長 部長 電源".split(" ")

if is_supervisor():    
    results = flatten(results)
    results.sort(key=lambda x: x["day"])
    headers = ["day",] + ["%02d:00:00" % (x,) for x in range(24)]
    column_headers = dict(map(lambda h: (h,h,), headers))
    for term in terms:
        this_term = [column_headers,]
        for day in results:
            one_day = day[term]
            one_day["day"] = day["day"]
            this_term.append(one_day)
        file = project.open_result_file("term-searches-2013-09-12/" + term + ".csv")(headers) 
        file.writerows(this_term)
        # file.close()
