#!/usr/bin/python
# coding=utf8
import sqlite3
import hashlib

from utility import *
import project

def add_segment_column(filename):
    # create new db file
    input_db = sqlite3.connect(filename)
    input_db.text_factory = str
    input_cursor = input_db.cursor()
    output_db = sqlite3.connect(
        os.environ["SCRATCH"] + "/sendai-segmented/" + date_from(filename) + ".db"
    )
    output_db.text_factory = str
    output_cursor = output_db.cursor()
    output_cursor.execute("""
        CREATE TABLE IF NOT EXISTS tweets (
            id INTEGER PRIMARY KEY,
            in_reply_to TEXT,
            body TEXT,
            body_hash TEXT,
            segmented_body TEXT, 
            username TEXT,
            nickname TEXT,
            metadata TEXT,
            source_url TEXT,
            retweeted_url TEXT,
            retweeted_id TEXT,
            url TEXT,
            twitter_id TEXT,
            date TEXT,
            geo TEXT,
            lang TEXT,
            img_url TEXT,
            author_id TEXT,
            rel_flag INTEGER
        );
    """)
    output_db.commit()
    output_rows = []
    counter = 0
    for row in input_cursor.execute("""
            SELECT id, in_reply_to, body, username, nickname,
                metadata, source_url, retweeted_url, retweeted_id,
                url, twitter_id, date, geo, lang, img_url,
                author_id, rel_flag
            FROM tweets
        """):
        # iterate through each tweet
        # compute md5 hash
        md5 = hashlib.md5()
        md5.update(row[2])
        output_rows.append([
            row[0], row[1], row[2], md5.digest(), row[3], row[4],
            row[5], row[6], row[7], row[8], row[9], row[10], row[11],
            row[12], row[13], row[14], row[15], row[16],
        ])
        # create new row in db
        # for each 10000 rows, commit
        if counter % 10000 == 0:
            output_cursor.executemany("""INSERT INTO tweets (
                    id, in_reply_to, body, body_hash, username,
                    nickname, metadata, source_url, retweeted_url,
                    retweeted_id, url, twitter_id, date,
                    geo, lang, img_url, author_id, rel_flag
                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                 ?, ?, ?, ?, ?, ?, ?, ?) """, output_rows)
            output_db.commit()
            output_rows = []
            counter = 0
        counter += 1
    input_cursor.close()
    output_cursor.close()


files = distribute(project.DATASET, add_segment_column)
if is_supervisor():
    # reduce part
    pass
