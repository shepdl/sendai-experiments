#!/usr/bin/python
import sqlite3
import csv

import project
from utility import *


def most_common_tweets(filename):
    # logger = project.logger_for("most_common_tweets")
    # logger.debug("Starting %s" % (filename,))
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()
    data = {}
    for row in cursor.execute(""" SELECT body, COUNT(*) AS total FROM tweets
            GROUP BY body HAVING COUNT(*) > 1
            """):
        data[row[0]] = int(row[1]) 
    cursor.close()
    conn.close()
    # logger.debug("Finished %s" % (filename,))
    return {"date" : date_from(filename), "data":  data,}

def main():
    """
    OUTPUT:
    CSV with format:
    Message,Count
    """
    # logger = logger_for("most_common_tweets")
    # logger.debug("Starting ...")
    # logger.debug(project.DATASET)
    results = distribute(project.DATASET, most_common_tweets)
    # logger.debug("Finished processing all files. Generating results file.")
    for data in results:
        # file = csv.writer(open(os.path.join(["$HOME", "sendai-experiments", "results", data["date"],], "w")))
        file = csv.writer(project.open_result_file("most-common-tweets-%s.csv", data["date"]))
        for message, count in data["data"].iteritems():
            file.writerow([message, count,])
        file.close()

if __name__ == "__main__":
    main()

