#!/usr/bin/python

import glob
import sqlite3

import project
# from utility import *

# files = flatten(files)
files = glob.glob("results/hashtag-counts/*.db")
print files
output = project.open_result_file("hashtag-counts-master.db")
output_cursor = output.cursor()
output_cursor.execute("""
    CREATE TABLE IF NOT EXISTS intermediary (tag TEXT, total INTEGER)
""")
output_cursor.execute("""
    CREATE TABLE IF NOT EXISTS results (tag TEXT, total INTEGER)
""")
output.commit()
for file in files:
    print file
    # input = project.open_result_file(file, "r")
    input = sqlite3.connect(file)
    input_cursor = input.cursor()
    for row in input_cursor.execute("""
        SELECT tag, SUM(total) FROM results GROUP BY tag
    """):
        output_cursor.execute("""
            INSERT INTO intermediary (tag, total) VALUES(?, ?)
        """, (row[0], row[1],)
        )
output.commit()
result_cursor = output.cursor()
for row in output_cursor.execute("""
    SELECT tag, SUM(total) FROM intermediary GROUP BY tag
"""):
    result_cursor.execute("""
        INSERT INTO results(tag, total) VALUES (?, ?)
    """, (row[0], row[1],))
output.commit()
output_cursor.execute(""" DROP TABLE intermediary """)
output_cursor.execute(""" VACUUM """)
output.commit()
output_cursor.close()
