#!/bin/csh -f
#  find_terms_2013-09-12.py.cmd
#
#  UGE job for find_terms_2013-09-12.py built Thu Sep 12 12:12:33 PDT 2013
#
#  The following items pertain to this script
#  Use current working directory
#$ -cwd
#  input           = /dev/null
#  output          = /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog
#$ -o /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog.$JOB_ID
#  error           = Merged with joblog
#$ -j y
#  The following items pertain to the user program
#  user program    = /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py
#  arguments       = 
#  program input   = Specified by user program
#  program output  = Specified by user program
#  Parallelism:  25-way parallel
#  Resources requested
#$ -pe dc* 25
#$ -l h_data=2048M,h_rt=8:00:00
# # #
#
#  Name of application for log
#$ -v QQAPP=openmpi
#  Email address to notify
#$ -M dshepard@mail
#  Notify at beginning and end of job
#$ -m bea
#  Job is not rerunable
#$ -r n
#  Uncomment the next line to have your environment variables used by SGE
#$ -V
#
# Initialization for mpi parallel execution
#
  unalias *
  set qqversion = 
  set qqapp     = "openmpi parallel"
  set qqptasks  = 25
  set qqidir    = /u/home/d/dshepard/sendai-experiments
  set qqjob     = find_terms_2013-09-12.py
  set qqodir    = /u/home/d/dshepard/sendai-experiments
  cd     /u/home/d/dshepard/sendai-experiments
  source /u/local/bin/qq.sge/qr.runtime
  if ($status != 0) exit (1)
#
  echo "UGE job for find_terms_2013-09-12.py built Thu Sep 12 12:12:33 PDT 2013"
  echo ""
  echo "  find_terms_2013-09-12.py directory:"
  echo "    "/u/home/d/dshepard/sendai-experiments
  echo "  Submitted to UGE:"
  echo "    "$qqsubmit
  echo "  'scratch' directory (on each node):"
  echo "    $qqscratch"
  echo "  find_terms_2013-09-12.py 25-way parallel job configuration:"
  echo "    $qqconfig" | tr "\\" "\n"
#
  echo ""
  echo "find_terms_2013-09-12.py started on:   "` hostname -s `
  echo "find_terms_2013-09-12.py started at:   "` date `
  echo ""
#
# Run the user program
#

  source /u/local/Modules/default/init/modules.csh
  module load intel/11.1
  module load openmpi/1.4

  echo find_terms_2013-09-12.py -nt 25 "" \>\& find_terms_2013-09-12.py.output.$JOB_ID
  echo ""

  setenv PATH /u/local/bin:$PATH

  time mpiexec -n 25 -machinefile $QQ_NODEFILE  \
         /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py  >& /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.output.$JOB_ID 

  echo ""
  echo "find_terms_2013-09-12.py finished at:  "` date `

#
# Cleanup after mpi parallel execution
#
  source /u/local/bin/qq.sge/qr.runtime
#
  echo "-------- /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog.$JOB_ID --------" >> /u/local/apps/queue.logs/openmpi.log.parallel
 if (`wc -l /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog.$JOB_ID  | awk '{print $1}'` >= 1000) then
        head -50 /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
        echo " "  >> /u/local/apps/queue.logs/openmpi.log.parallel
        tail -10 /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  else
        cat /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  endif
#  cat            /u/home/d/dshepard/sendai-experiments/find_terms_2013-09-12.py.joblog.$JOB_ID           >> /u/local/apps/queue.logs/openmpi.log.parallel
  exit (0)
