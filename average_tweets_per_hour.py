#!/usr/bin/python
import os
import glob

import sqlite3
import csv

from utility import *
import project

# Absolute counts:
# Hour Day1 Day2  ... Totals
#
# Proportions:
# Hour Day1 Day 2
#
# Daily percentages:
# Hour AverageNumber AveragePercentage 
#
def main():
    print "Average tweets per hour ..." 
    results = distribute(project.DATASET, tweets_per_hour)
    # print "Node %s results length: %s" % (rank(), len(results),)
    if is_supervisor():
        print "Entering supervisor"
        # flatten data, since we assume there are more files than nodes, and thus
        # each node returns a list of result sets
        # print "Pre-flattened"
        # print results
        # print "Flattening ..."
        results = flatten(results)
        # print "Post-flatened"
        # print results
        data = {}
        # for item in results:
        #     for part in item:
        #         data[item["day"]] = part
        # hourly_counts = {}
        # organize
        column_headers = ["day",] + ["%02d:00:00" % (x,) for x in range(24)]  + ["total",]
        results.sort(key=lambda r:r["day"])
        headers_row = dict(map(lambda x: (x,x,), column_headers))
        results = [headers_row,] + results
        # hourly_counts = {} 
        # for day in data:
        #     # technically, this includes "day", which we do want as a column
        #     for hour in column_headers:
        #         if hour not in hourly_counts:
        #             hourly_counts[hour] = {}
        #         # day_reading = hourly_counts[hour]
        #         # if day not in day_reading:
        #         #     day_reading[day] = {}
        #         # day_reading[day] = data[day][hour]
        #         hourly_counts[hour][day] = data[day][hour]
        print "Headers:"
        print column_headers
        out_file = project.open_result_file("hourly-tweet-totals.csv")(column_headers)
        # out_file.writeheader()
        # out_file.writerows(hourly_counts)
        out_file.writerows(results)
        # out_file.close()
    
def tweets_per_hour(filename):
    file = sqlite3.connect(filename)
    cursor = file.cursor()
    daily_sum = 0
    hourly_totals = {"day" : date_from(filename),}
    for row in cursor.execute("""SELECT strftime("%H:00:00", date) AS hour,
                COUNT(*) AS total FROM tweets GROUP BY strftime("%H", date)
            """):
        daily_sum += int(row[1])
        hour = row[0] #.split(" ")[1]
        hourly_totals[hour] = int(row[1])
    hourly_totals["total"] = daily_sum
    return hourly_totals

if __name__ == "__main__":
    main()
