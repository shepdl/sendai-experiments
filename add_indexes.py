#!/usr/bin/python
import os
import sqlite3

import project
from utility import *

def debug_printer(filename):
    def fn(message):
        print filename + " " + message
    return fn

def add_indexes(filename):
    debug = debug_printer(filename)
    debug("We are in ...")
    output_file = sqlite3.connect(filename)
    output_cursor = output_file.cursor()
    debug("Indexing usernames ...")
    output_cursor.execute("""
        CREATE INDEX IF NOT EXISTS IDX_TWEETS_NICKNAMES ON tweets (username);
    """)
    # debug("Indexing message body ...")
    # output_cursor.execute("""
    #     CREATE INDEX IF NOT EXISTS IDX_TWEETS_BODY ON tweets(body);
    # """)

    #debug("Indexing dates ...")
    #output_cursor.execute("""
    #    CREATE INDEX IF NOT EXISTS IDX_TWEETS_DATE ON tweets(date);
    #""")
    output_cursor.close()

# os.mkdir(os.path.join(os.environ["SCRATCH"], "sendai-data-new"))
distribute(project.DATASET, add_indexes)
print "All done"
