import os

from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

files = os.listdir("$SCRATCH/beginning_data")
if rank == 0:
    node_files = MPI.scatter(files)
    map(import_file, node_files)
