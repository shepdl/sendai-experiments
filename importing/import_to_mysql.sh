for f in {10..31}
do 
    tar -xvf jishin.tar "201103$f.zip" 
    unzip "201103$f.zip" 
    tr < "201103$f.csv" -d '\000' > "201103$f.csv.clean" 
    echo "Cleaned 201103$f.csv; importing ..." 
    rm "201103$f.zip" 
    rm "201103$f.csv" 
    python import_file.py "201103$f.csv.clean" 
    rm "201103$f.csv.clean" 
done
for f in 1 2 
do 
    tar -xvf jishin.tar "2011040$f.zip" 
    unzip "2011040$f.zip" 
    tr < "2011040$f.csv" -d '\000' > "2011040$f.csv.clean" 
    echo "Cleaned 2011040$f.csv; importing ..." 
    rm "2011040$f.zip" 
    rm "2011040$f.csv" 
    python import_file.py "2011040$f.csv.clean" 
    rm "2011040$f.csv.clean" 
done


tar -xvf jishin.tar "20110319.zip" 
unzip "20110319.zip" 
tr < "20110319.csv" -d '\000' > "20110319.csv.clean" 
echo "Cleaned 20110319.csv; importing ..." 
rm "20110319.zip" 
rm "20110319.csv" 
python import_file.py "20110319.csv.clean" 
rm "20110319.csv.clean" 

