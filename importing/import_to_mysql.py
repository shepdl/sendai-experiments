import tarfile
import sqlite3
import MySQLdb

archive = tarfile.open("jishin.tar")
for file in archive:
    source_db = sqlite3.connect(archive.extractfile(file))
    target_db = MySQLdb.connect(
        user="root", db="jishin", 
        use_unicode=True, charset="utf8"
    )
    source_cursor = source_db.cursor()
    target_cursor = target_db.cursor()
    target_cursor.execute(""" 
        CREATE TABLE tweets (
            id bigint auto_increment,
            twitter_id VARCHAR(50),
            username varchar(50),
            nickname varchar(50),
            body TEXT,
            metadata TEXT,
            in_reply_to varchar(40),
            source_url VARCHAR(255),
            retweeted_url VARCHAR(255),
            retweeted_id VARCHAR(40),
            url VARCHAR(100),
            date DATETIME,
            geo POINT,
            lang varchar(5),
            img_url VARCHAR(100),
            author_id VARCHAR(50),
            rel_flag tinyint(1),
            PRIMARY KEY (id)
        ) DEFAULT CHARSET=utf8;
    """)
    counter = 0
    for row in source_cursor.execute("""SELECT 
        twitter_id, url, nickname, body, metadata
        in_reply_to, source_url, retweeted_url,
        retweeted_id, url, date, geo, lang, img_url,
        author_id, rel_flag
    FROM tweets """):
        url = row[1]
        url_pieces = url.split("/")
        username = url
        if len(url_pieces) > 3:
            username = url_pieces[3]
        counter += 1
        target_cursor.execute("""
            INSERT INTO tweets (twitter_id,
            username, nickname, body, metadata,
            in_reply_to, source_url, retweeted_url,
            retweeted_id, url, date, geo, lang,
            img_url, author_id, rel_flag) 
            VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s
            %s,%s,%s,%s,%s)
        """, (row[0], username, row[2], row[3], row[4],
              row[5], row[6], row[7], row[8], url, row[9],
              row[10], row[11], row[12], row[13], row[14],
              row[15],
          )
        )
        if counter == 10000:
            target_db.commit()
            counter = 0
    target_db.commit()
    source_cursor.close()
    target_cursor.close()
    

