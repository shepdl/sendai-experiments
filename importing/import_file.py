import tarfile
import sqlite3
import os
import sys
import glob
import csv
import MySQLdb

# archive = tarfile.open("jishin-clean.tar")
target_db = MySQLdb.connect(
    user="root", db="jishin",
    use_unicode=True, charset="utf8"
)
def commit(target_cursor,  target_db, data):
    target_cursor.executemany("""
        INSERT INTO tweets(twitter_id,
        `username`, nickname, body, metadata,
        in_reply_to, source_url, retweeted_url,
        retweeted_id, url, `date`, -- geo,
        lang,
        img_url, author_id, rel_flag)
        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, -- s,
        %s, %s, %s, %s, %s)
    """, data)
    target_db.commit()

# source_cursor = source_db.cursor()
target_cursor = target_db.cursor()
target_cursor.execute("""
    CREATE TABLE IF NOT EXISTS tweets (
        id bigint auto_increment,
        twitter_id VARCHAR(50),
        username varchar(50),
        nickname varchar(50),
        body TEXT,
        metadata TEXT,
        in_reply_to varchar(80),
        source_url VARCHAR(255),
        retweeted_url VARCHAR(255),
        retweeted_id VARCHAR(40),
        url VARCHAR(100),
        date DATETIME,
        -- geo POINT,
        lang varchar(5),
        img_url VARCHAR(255),
        author_id VARCHAR(50),
        rel_flag tinyint(1),
        PRIMARY KEY (id)
    ) DEFAULT CHARSET=utf8;
""")
print "Importing %s" % (file,)
# for file in archive:
# fs_file = archive.extractall(members=[file])
# source_db = sqlite3.connect(archive.extractfile(file))
# source_db = sqlite3.connect(file.name)
counter = 0
data = []
for row in csv.reader(open(sys.argv[1], "rb")):
    fields = map(lambda i: i.split("=", 1)[1], row)
    url = row[1]
    url_pieces = url.split("/")
    username = url
    if len(fields) < 15:
        print "Row too short; skipping ..."
        continue
    # geo = fields[13]
    if len(url_pieces) > 3:
        username = url_pieces[3]
    data.append((
        fields[9].split("/")[-1], # twitter_id
        username,  # username
        fields[5], # nickname
        fields[2], # body
        fields[6], # metadata
        fields[1], # in_reply_to
        fields[7], # source_url
        fields[8], # retweeted_url
        fields[8].split("/")[-1], # retweeted id
        fields[9], # url
        fields[10], # date
        # fields[12], # geo
        fields[13], # lang
        fields[14], # img_url
        fields[15], # author_id
        fields[16], # rel_flag
    ))
    counter += 1
    if counter == 100000:
        commit(target_cursor, target_db, data)
        data = []
        counter = 0
commit(target_cursor, target_db, data)


target_cursor.close()
