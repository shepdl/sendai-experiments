import csv
import sqlite3

def import_file(filename, output_filename=None):
    # set up database
    new_filename = output_filename if output_filename is not None else filename.split("/")[-1]
    conn = sqlite3.connect(new_filename)
    cursor = conn.cursor()
    conn.text_factory = str
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS tweets (
            id INTEGER PRIMARY KEY,
            in_reply_to TEXT,
            body TEXT,
            nickname TEXT,
            metadata TEXT,
            source_url TEXT,
            retweeted_url TEXT,
            retweeted_id TEXT,
            url TEXT,
            twitter_id TEXT,
            date TEXT,
            geo TEXT,
            lang TEXT,
            img_url TEXT,
            author_id TEXT,
            rel_flag INTEGER
        )
    """)

    counter = 0
    data = []
    for line in csv.reader(open(filename, "rb")):
        counter += 1
        fields = map(lambda i: i.split("=", 1)[1], line)
        data.append((
            fields[1], # in_reply_to
            fields[2], # body
            fields[5], # nickname
            fields[6], # metadata
            fields[7], # source_url
            fields[8], # retweeted_url
            fields[8].split("/")[-1], # retweeted id
            fields[9], # url
            fields[9].split("/")[-1], # twitter_id
            fields[10], # date
            fields[12], # geo
            fields[13], # lang
            fields[14], # img_url
            fields[15], # author_id
            fields[16], # rel_flag
        ))
        if counter == 10000:
            commit(conn, cursor, data)

            data = []
            counter = 0

    # final commit
    if len(data) > 0:
        commit(conn, cursor, data)
    conn.close()

def commit(conn, cursor, data):
    cursor.executemany("""INSERT INTO tweets (
            in_reply_to, body, nickname, metadata, source_url,
             retweeted_url, retweeted_id, url, twitter_id,
             date, geo, lang, img_url, author_id, rel_flag
         ) VALUES (?,?,?,?,?, ?,?,?,?, ?,?,?,?,?,?)
         """,
         data
     )
    conn.commit()

