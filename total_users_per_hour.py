#!/usr/bin/python

import csv
import sqlite3
import project
from utility import *

def total_users_per_hour(filename):
    print "Entering %s" % (filename,)
    file = sqlite3.connect(filename)
    cursor = file.cursor()
    hourly_totals = { "day": date_from(filename), }
    for row in cursor.execute("""
            SELECT strftime("%H:00:00", date) AS hour, 
                COUNT(DISTINCT username) AS total FROM tweets
            GROUP BY strftime("%Y-%m-%d-%H", date)
        """):
        hourly_totals[row[0]] = int(row[1])
    cursor.close()
    file.close()
    print "Finished %s" % (filename,)
    print hourly_totals
    return hourly_totals

def main():
    results = distribute(project.DATASET, total_users_per_hour)
    if is_supervisor():
        results = flatten(results)
        results.sort(key=lambda r:r[0])
        hourly_counts = {}
        # organize
        column_headers = ["day",] + ["%02d:00:00" % (x,) for x in range(24)]
        results = [column_headers,] + results
        hourly_counts = {} 
        out_file = project.open_result_file("hourly-user-totals.csv")(column_headers)
        out_file.writerows(results)
        out_file.close()

if __name__ == "__main__":
    main()
