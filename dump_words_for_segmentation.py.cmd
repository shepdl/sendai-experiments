#!/bin/csh -f
#  dump_words_for_segmentation.py.cmd
#
#  UGE job for dump_words_for_segmentation.py built Thu Jun 20 10:12:05 PDT 2013
#
#  The following items pertain to this script
#  Use current working directory
#$ -cwd
#  input           = /dev/null
#  output          = /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog
#$ -o /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog.$JOB_ID
#  error           = Merged with joblog
#$ -j y
#  The following items pertain to the user program
#  user program    = /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py
#  arguments       = 
#  program input   = Specified by user program
#  program output  = Specified by user program
#  Parallelism:  25-way parallel
#  Resources requested
#$ -pe dc* 25
#$ -l h_data=1024M,h_rt=8:00:00
# # #
#
#  Name of application for log
#$ -v QQAPP=openmpi
#  Email address to notify
#$ -M dshepard@mail
#  Notify at beginning and end of job
#$ -m bea
#  Job is not rerunable
#$ -r n
#  Uncomment the next line to have your environment variables used by SGE
#$ -V
#
# Initialization for mpi parallel execution
#
  unalias *
  set qqversion = 
  set qqapp     = "openmpi parallel"
  set qqptasks  = 25
  set qqidir    = /u/home/d/dshepard/sendai-experiments
  set qqjob     = dump_words_for_segmentation.py
  set qqodir    = /u/home/d/dshepard/sendai-experiments
  cd     /u/home/d/dshepard/sendai-experiments
  source /u/local/bin/qq.sge/qr.runtime
  if ($status != 0) exit (1)
#
  echo "UGE job for dump_words_for_segmentation.py built Thu Jun 20 10:12:05 PDT 2013"
  echo ""
  echo "  dump_words_for_segmentation.py directory:"
  echo "    "/u/home/d/dshepard/sendai-experiments
  echo "  Submitted to UGE:"
  echo "    "$qqsubmit
  echo "  'scratch' directory (on each node):"
  echo "    $qqscratch"
  echo "  dump_words_for_segmentation.py 25-way parallel job configuration:"
  echo "    $qqconfig" | tr "\\" "\n"
#
  echo ""
  echo "dump_words_for_segmentation.py started on:   "` hostname -s `
  echo "dump_words_for_segmentation.py started at:   "` date `
  echo ""
#
# Run the user program
#

  source /u/local/Modules/default/init/modules.csh
  module load intel/11.1
  module load openmpi/1.4

  echo dump_words_for_segmentation.py -nt 25 "" \>\& dump_words_for_segmentation.py.output.$JOB_ID
  echo ""

  setenv PATH /u/local/bin:$PATH

  time mpiexec -n 25 -machinefile $QQ_NODEFILE  \
         /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py  >& /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.output.$JOB_ID 

  echo ""
  echo "dump_words_for_segmentation.py finished at:  "` date `

#
# Cleanup after mpi parallel execution
#
  source /u/local/bin/qq.sge/qr.runtime
#
  echo "-------- /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog.$JOB_ID --------" >> /u/local/apps/queue.logs/openmpi.log.parallel
 if (`wc -l /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog.$JOB_ID  | awk '{print $1}'` >= 1000) then
        head -50 /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
        echo " "  >> /u/local/apps/queue.logs/openmpi.log.parallel
        tail -10 /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  else
        cat /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  endif
#  cat            /u/home/d/dshepard/sendai-experiments/dump_words_for_segmentation.py.joblog.$JOB_ID           >> /u/local/apps/queue.logs/openmpi.log.parallel
  exit (0)
