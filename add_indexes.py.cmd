#!/bin/csh -f
#  add_indexes.py.cmd
#
#  UGE job for add_indexes.py built Thu Mar 14 14:06:15 PDT 2013
#
#  The following items pertain to this script
#  Use current working directory
#$ -cwd
#  input           = /dev/null
#  output          = /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog
#$ -o /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog.$JOB_ID
#  error           = Merged with joblog
#$ -j y
#  The following items pertain to the user program
#  user program    = /u/home/campus/dshepard/sendai-experiments/add_indexes.py
#  arguments       = 
#  program input   = Specified by user program
#  program output  = Specified by user program
#  Parallelism:  8-way parallel
#  Resources requested
#$ -pe shared 8
#$ -l h_data=2048M,h_rt=8:00:00
# # #
#
#  Name of application for log
#$ -v QQAPP=openmpi
#  Email address to notify
#$ -M dshepard@mail
#  Notify at beginning and end of job
#$ -m bea
#  Job is not rerunable
#$ -r n
#  Uncomment the next line to have your environment variables used by SGE
#$ -V
#
# Initialization for mpi parallel execution
#
  unalias *
  set qqversion = 
  set qqapp     = "openmpi parallel"
  set qqptasks  = 8
  set qqidir    = /u/home/campus/dshepard/sendai-experiments
  set qqjob     = add_indexes.py
  set qqodir    = /u/home/campus/dshepard/sendai-experiments
  cd     /u/home/campus/dshepard/sendai-experiments
  source /u/local/bin/qq.sge/qr.runtime
  if ($status != 0) exit (1)
#
  echo "UGE job for add_indexes.py built Thu Mar 14 14:06:15 PDT 2013"
  echo ""
  echo "  add_indexes.py directory:"
  echo "    "/u/home/campus/dshepard/sendai-experiments
  echo "  Submitted to UGE:"
  echo "    "$qqsubmit
  echo "  'scratch' directory (on each node):"
  echo "    $qqscratch"
  echo "  add_indexes.py 8-way parallel job configuration:"
  echo "    $qqconfig" | tr "\\" "\n"
#
  echo ""
  echo "add_indexes.py started on:   "` hostname -s `
  echo "add_indexes.py started at:   "` date `
  echo ""
#
# Run the user program
#

  source /u/local/Modules/default/init/modules.csh
  module load intel/11.1
  module load openmpi/1.4

  echo add_indexes.py -nt 8 "" \>\& add_indexes.py.output.$JOB_ID
  echo ""

  setenv PATH /u/local/bin:$PATH

  time mpiexec -n 8 -machinefile $QQ_NODEFILE  \
         /u/home/campus/dshepard/sendai-experiments/add_indexes.py  >& /u/home/campus/dshepard/sendai-experiments/add_indexes.py.output.$JOB_ID 

  echo ""
  echo "add_indexes.py finished at:  "` date `

#
# Cleanup after mpi parallel execution
#
  source /u/local/bin/qq.sge/qr.runtime
#
  echo "-------- /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog.$JOB_ID --------" >> /u/local/apps/queue.logs/openmpi.log.parallel
 if (`wc -l /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog.$JOB_ID  | awk '{print $1}'` >= 1000) then
        head -50 /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
        echo " "  >> /u/local/apps/queue.logs/openmpi.log.parallel
        tail -10 /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  else
        cat /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  endif
#  cat            /u/home/campus/dshepard/sendai-experiments/add_indexes.py.joblog.$JOB_ID           >> /u/local/apps/queue.logs/openmpi.log.parallel
  exit (0)
