#!/usr/bin/python
import os
import sqlite3

import project
from utility import *

def debug_printer(filename):
    def fn(message):
        print filename + " " + message
    return fn

def add_username_column(filename):
    file = sqlite3.connect(filename)
    debug = debug_printer(filename)
    debug("Creating new DB ...")
    cursor = file.cursor()
    output_file = sqlite3.connect(os.path.join(os.environ["SCRATCH"], "sendai-data-new", date_from(filename) + ".db" ))
    output_cursor = output_file.cursor()
    output_cursor.execute(""" DROP TABLE IF EXISTS tweets """)
    output_cursor.execute("""
        CREATE TABLE tweets (
            id INTEGER PRIMARY KEY,
            in_reply_to TEXT,
            body TEXT,
            username TEXT,
            nickname TEXT,
            metadata TEXT,
            source_url TEXT,
            retweeted_url TEXT,
            retweeted_id TEXT,
            url TEXT,
            twitter_id TEXT,
            date TEXT,
            geo TEXT,
            lang TEXT,
            img_url TEXT,
            author_id TEXT,
            rel_flag INTEGER
        );
    """)
    counter = 0
    debug("Copying rows")
    for row in cursor.execute("""
        SELECT id, in_reply_to, body, nickname, metadata, source_url,
            retweeted_url, retweeted_id, url, twitter_id, date, geo,
            lang, img_url, author_id, rel_flag
        FROM tweets
    """):
        output_cursor.execute("""
            INSERT INTO tweets (in_reply_to, body, username, nickname, metadata, source_url, 
                    retweeted_url, retweeted_id, url, twitter_id, date, geo,
                    lang, img_url, author_id, rel_flag) 
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
        """, (row[1], row[2], user_from(row[8]), row[3], row[4], row[5],
                row[6], row[7], row[8], row[9], row[10], row[11], row[12],
                row[13], row[14], row[15],)
        )
        counter += 1
        if counter == 100000:
            output_file.commit()
    debug("Rows finished. Committing ...")
    output_file.commit()

    # debug("Indexing nicknames ...")
    # output_cursor.execute("""
    #     CREATE INDEX IDX_TWEETS_NICKNAMES ON tweets (username);
    # """)
    # debug("Indexing message body ...")
    # output_cursor.execute("""
    #     CREATE INDEX IDX_TWEETS_BODY ON tweets(body);
    # """)
    # debug("Indexing dates ...")
    # output_cursor.execute("""
    #     CREATE INDEX IDX_TWEETS_DATE ON tweets(date);
    # """)
    cursor.close()
    output_cursor.close()

# os.mkdir(os.path.join(os.environ["SCRATCH"], "sendai-data-new"))
distribute(project.DATASET, add_username_column)
print "All done"
