#!/bin/csh -f
#  total_tweets_per_user.py.cmd
#
#  UGE job for total_tweets_per_user.py built Tue Mar 12 22:56:53 PDT 2013
#
#  The following items pertain to this script
#  Use current working directory
#$ -cwd
#  input           = /dev/null
#  output          = /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog
#$ -o /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog.$JOB_ID
#  error           = Merged with joblog
#$ -j y
#  The following items pertain to the user program
#  user program    = /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py
#  arguments       = 
#  program input   = Specified by user program
#  program output  = Specified by user program
#  Parallelism:  9-way parallel
#  Resources requested
#$ -pe dc* 9
#$ -l h_data=1024M,h_rt=8:00:00
# # #
#
#  Name of application for log
#$ -v QQAPP=openmpi
#  Email address to notify
#$ -M dshepard@mail
#  Notify at beginning and end of job
#$ -m bea
#  Job is not rerunable
#$ -r n
#  Uncomment the next line to have your environment variables used by SGE
#$ -V
#
# Initialization for mpi parallel execution
#
  unalias *
  set qqversion = 
  set qqapp     = "openmpi parallel"
  set qqptasks  = 9
  set qqidir    = /u/home/campus/dshepard/sendai-experiments
  set qqjob     = total_tweets_per_user.py
  set qqodir    = /u/home/campus/dshepard/sendai-experiments
  cd     /u/home/campus/dshepard/sendai-experiments
  source /u/local/bin/qq.sge/qr.runtime
  if ($status != 0) exit (1)
#
  echo "UGE job for total_tweets_per_user.py built Tue Mar 12 22:56:53 PDT 2013"
  echo ""
  echo "  total_tweets_per_user.py directory:"
  echo "    "/u/home/campus/dshepard/sendai-experiments
  echo "  Submitted to UGE:"
  echo "    "$qqsubmit
  echo "  'scratch' directory (on each node):"
  echo "    $qqscratch"
  echo "  total_tweets_per_user.py 9-way parallel job configuration:"
  echo "    $qqconfig" | tr "\\" "\n"
#
  echo ""
  echo "total_tweets_per_user.py started on:   "` hostname -s `
  echo "total_tweets_per_user.py started at:   "` date `
  echo ""
#
# Run the user program
#

  source /u/local/Modules/default/init/modules.csh
  module load intel/11.1
  module load openmpi/1.4

  echo total_tweets_per_user.py -nt 9 "" \>\& total_tweets_per_user.py.output.$JOB_ID
  echo ""

  setenv PATH /u/local/bin:$PATH

  time mpiexec -n 9 -machinefile $QQ_NODEFILE  \
         /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py  >& /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.output.$JOB_ID 

  echo ""
  echo "total_tweets_per_user.py finished at:  "` date `

#
# Cleanup after mpi parallel execution
#
  source /u/local/bin/qq.sge/qr.runtime
#
  echo "-------- /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog.$JOB_ID --------" >> /u/local/apps/queue.logs/openmpi.log.parallel
 if (`wc -l /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog.$JOB_ID  | awk '{print $1}'` >= 1000) then
        head -50 /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
        echo " "  >> /u/local/apps/queue.logs/openmpi.log.parallel
        tail -10 /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  else
        cat /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog.$JOB_ID >> /u/local/apps/queue.logs/openmpi.log.parallel
  endif
#  cat            /u/home/campus/dshepard/sendai-experiments/total_tweets_per_user.py.joblog.$JOB_ID           >> /u/local/apps/queue.logs/openmpi.log.parallel
  exit (0)
