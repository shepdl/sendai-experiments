#!/usr/bin/python
#coding=utf8
import sqlite3

from utility import *
import project

def user_networks(filename):
    # map part
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()
    users = {}
    for row in cursor.execute("""
            SELECT username, body
            FROM tweets
            WHERE body LIKE '%%@%%'
        """):
        source_user = row[0]
        for user in users_in(row[1]):
            users.setdefault(source_user, {})
            # users[source_user].append(user)
            users[source_user].setdefault(user, 0)
            users[source_user][user] += 1
    out = project.open_result_file("user_retweet_networks/%s.csv" % date_from(filename))(["source", "target", "weight",])
    out_users = []
    for source in users:
        for target, weight in users[source].iteritems():
            if weight > 1:
                out_users.append({"source" : source, "target":  target,  "weight" : weight,})
    out.writerows(out_users)

files = distribute(project.DATASET, user_networks)
