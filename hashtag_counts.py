#!/usr/bin/python

import sqlite3

import project
from utility import *

def hashtag_counts(filename):
    input_db = sqlite3.connect(filename)
    input_cursor = input_db.cursor()
    output_db = project.open_temp_file(date_from(filename) + ".db")
    hashtag_counts = {}
    for row in input_cursor.execute(""" 
            SELECT strftime(date, "%H"), body FROM tweets WHERE body LIKE '%#%'
        """):
        hashtag_counts.setdefault(row[0], {})
        for tag in tags_from(row[1]):
            hashtag_counts[row[0]].setdefault(tag, 0)
            hashtag_counts[row[0]][tag] += 1
    for hour in hashtag_counts:
        output = project.open_result_file("hashtag-counts/%s.db" % (date_from(filename),))
        cursor = output.cursor()
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS results(hour INTEGER, tag TEXT, total INTEGER)
        """)
        output.commit()
        for tag, value in hashtag_counts[hour].iteritems():
            output.execute("""
                INSERT INTO results(hour, tag, total) VALUES(?, ?, ?)
            """, (hour, tag, value,)
            )
        output.commit()
    # return output.name

files = distribute(project.DATASET, hashtag_counts)
if is_supervisor():
    # files = flatten(files)
    # NOTE: there are two errors in this file. This one causes a silent failure
    # because ~ is not recoginzed by glob.
    files = glob.glob("~/sendai-experiments/results/hashtag-counts/*.db")
    output = project.open_result_file("hashtag-counts-master.db")
    output_cursor = output.cursor()
    output_cursor.execute("""
        CREATE TABLE IF NOT EXISTS intermediary (tag TEXT, total INTEGER)
    """)
    output_cursor.execute("""
        CREATE TABLE IF NOT EXISTS results (tag TEXT, total INTEGER)
    """)
    output.commit()
    for file in files:
        # NOTE: this is the second error. open_result_file needs a different
        # path. I didn't bother trying to figure out which one and just wrote
        # hashtag_counts_summary.py instead.
        input = project.open_result_file(file, "r")
        input_cursor = input.cursor()
        for row in input_cursor.execute("""
            SELECT tag, SUM(total) FROM results GROUP BY tag
        """):
            output_cursor.execute("""
                INSERT INTO intermediary (tag, total) VALUES(?, ?)
            """, (row[0], row[1],)
            )
    output.commit()
    result_cursor = output.cursor()
    for row in output_cursor.execute("""
        SELECT tag, SUM(total) FROM intermediary GROUP BY tag
    """):
        result_cursor.execute("""
            INSERT INTO results(tag, total) VALUES (?, ?)
        """, (row[0], row[1],))
    output.commit()
    output_cursor.execute(""" DROP TABLE intermediary """)
    output_cursor.execute(""" VACUUM """)
    output.commit()
    output_cursor.close()
