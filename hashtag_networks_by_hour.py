#!/usr/bin/python

import project
from utility import *
from NetworkX import nx

class AffinityEdge(object):
    def __init__(self, nodes, weight=0):
        self.nodes = nodes
        self.weight = weight

    def __hash__(self):
        return "-".join(self.nodes) + "-" + str(self.weight)
        pass
    
    def __eq__(self, other):
        return self.__hash__() == other.__hash__()
        pass

    def inc(self, by=1):
        self.weight += by

    def __add__(self, other):
        return self.weight + other

    def __sub__(self, other):
        return self.weight - other

    def __mul__(self, other):
        return self.weight * other

    def __div__(self, other):
        return self.weight / other

class UpdatableGraph(nx.Graph):
    def __init__(self, *args, **kwargs):
        # TODO: superclass construction
        pass

    def update(self, node1, node2, weight_change=1):
        # check if edge exists; if not, create
        # otherwise, find old edge and replace
        if node1 in self:
            value = self[node1][node2]
            self[node1][node2]["weight"] += value + weight_change
        else:
            self.add_edge(node1, node2, weight=weight_change)
        

def hashtag_networks_by_hour(filename):
    """
        Generates a tag network for each hour.

        OUTPUT: One CSV edge list per hour.
    """
    graph = UpdatableGraph()
    last_hour = None
    for row in cursor.execute("""SELECT strftime("%H", tweeted_at) AS hour, body"""):
        if last_hour is None:
            last_hour = hour
        hour = row[0]
        if hour != last_hour:
            output_file = project.open_result_file("hashtag-networks-by-hour/%s-%s.csv" % (date_from(filename), last_hour,))
            for edge, weight in graph.iteritems():
                output_file.write("%s,%s,%s\n" % edge + (weight,))
            output_file.close()
            hour = last_hour
        for permutation in itertools.permutations(tags_from(row[1]), 2):
            graph.update(permutation[1], permutation[2])
    # TODO: write out graph
    return data

distribute(project.DATASET, hashtag_networks_by_hour)
