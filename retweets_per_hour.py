#!/usr/bin/python
import sqlite3

import project
from utility import *

def retweets_per_hour(filename):
    """
        INPUT: standard dataset
        OUTPUT: CSV file
    """
    print filename
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()
    results = {"day" : date_from(filename),}
    for row in cursor.execute("""
            SELECT strftime("%H:00:00", date), COUNT(*)
            FROM tweets
            WHERE body LIKE "%RT%"
            GROUP BY strftime("%H:00:00", date) 
        """):
        results[row[0]] = row[1]
    return results

results = distribute(project.DATASET, retweets_per_hour)

if is_supervisor():
    results = flatten(results)
    results.sort(key=lambda r:r["day"])
    hourly_counts = {}
    # organize
    column_headers = ["day",] + ["%02d:00:00" % (x,) for x in range(24)]
    csv_headers = dict(map(lambda h: (h,h,), column_headers))
    results = [csv_headers,] + results
    hourly_counts = {} 
    # for day in results:
    #     # technically, this includes "day", which we do want as a column
    #     for hour in column_headers:
    #         if hour not in hourly_counts:
    #             hourly_counts[hour] = {}
    #         day_reading = hourly_counts[hour]
    #         if day not in day_reading:
    #             day_reading[day] = {}
    #         day_reading[day] = results[day][hour]
    out_file = project.open_result_file("retweets-per-hour.csv")(column_headers)
    # out_file.writeheader()
    # out_file.writerows(hourly_counts)
    out_file.writerows(results)
    out_file.close()

