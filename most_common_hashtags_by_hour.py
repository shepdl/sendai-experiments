import sqlite3

from utility import *
import project

# print out hourly and daily reports
def most_common_hashtags_by_hour():
    """
        Extract most common hashtags for each hour.

        Output:
        Database with table of format Time,Tag,Count
    """
    data = {}
    for row in cursor.execute("""
            SELECT strftime("%Y-%m-%d %H:00:00", tweeted_at) AS hour, body
            """):
        data.setdefault(row["hour"], {})
        for tag in tags_in(row["body"]):
            data["hour"].setdefault(tag, 0)
            data["hour"][tag] += 1
    return data

def main():
    results = distribute(project.DATASET, most_common_hashtags_by_hour)
    data = []
    for node_set in results:
        for day_report in node_set:
            data.append(day_report)
    conn = sqlite3.connect(result_file("most_common_hashtags_by_hour.db"))
    # TODO: create table
    cursor = conn.cursor()
    query = """ INSERT INTO hashtags_by_hour (timestamp, tag, total) """ 
    for day in data:
        for timestamp in day:
            for tag, value in day[timestamp].iteritems():
                cursor.execute(query, (timestamp, tag, value,))
    cursor.close()
    conn.commit()
    conn.close()


