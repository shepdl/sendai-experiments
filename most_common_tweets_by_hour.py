def most_common_tweets_by_hour(cursor):
    """
    EXPECTED OUTPUT:
    Database with format
    Time,Message,Count
    """
    data = {}
    for row in cursor.execute("""
            SELECT strftime("%Y-%m-%d %H:00:00", tweeted_at) AS hour, 
                body, COUNT(*) AS total FROM tweets
            GROUP BY strftime("%Y-%m-%d %H:00:00", tweeted_at), body
            """):
        if row["hour"] not in data:
            data[row["hour"]] = {}
        hour = data[row["hour"]]
        hour[row["body"]] = int(row["total"]) 
    return data

