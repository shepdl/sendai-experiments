#!/usr/bin/python
# coding=utf8
import os
import sqlite3
import codecs

from utility import *
import project

def import_segments(filename):
    conn = sqlite3.connect(filename)
    # print "[%s] found database ..." % (filename,)
    segmented_file = codecs.open(
        os.path.join(
            os.environ["SCRATCH"],
            "sendai-segmented-scratch",
            date_from(filename) + "-tweet-dump.txt"
        ),
        encoding="utf-8"
    )
    # print "[%s] found file ..." % (filename,)
    conn.text_factory = str

    cursor = conn.cursor()
    rows = []
    counter = 0
    first = False
    for line in segmented_file:
        row_id, body = line.split(":", 1)
        row_id = row_id.strip()
        body = body.strip()
        rows.append((body, row_id,))
        counter += 1
        if counter >= 10000:
            counter = 0
            cursor.executemany("""
                UPDATE tweets SET segmented_body = ?
                WHERE id = ?
            """, rows)
            conn.commit()
            rows = []
            # if first:
            #    print "[%s] made first commit ..." % (filename,)
    conn.commit()
    segmented_file.close()
    cursor.close()
    conn.close()

files = distribute(project.DATASET_SEGMENTED, import_segments)
