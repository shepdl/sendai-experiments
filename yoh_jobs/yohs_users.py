import sqlite3
import sys
import codecs
import os
import glob

usernames = ["tranquil_dragon", "OfficialTEPCO", "kantei", "namicoaoto",]
username_files = {}
for name in usernames:
    username_files[name.lower()] = codecs.open(
        os.path.join(os.environ["HOME"], "sendai-experiments", "results", "yoh_users",
                "%s-from-%s-to-%s" % (name, sys.argv[1], sys.argv[2],)), 
        "w", encoding="utf8"
    )
    username_files[name.lower()].write("date,message\n")

for day in range(int(sys.argv[1]), int(sys.argv[2])):
# for db_file in glob.glob(os.path.join(os.environ["SCRATCH"], "sendai-segmented", "*.db")):
    db_file = os.path.join(os.environ["SCRATCH"], "sendai-segmented", "201103%02d.db" % (day,))
    db_file = os.path.join(os.environ["SCRATCH"], "sendai-segmented", "201104%02d.db" % (day,))
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    print "Searching for names in %s ..." % (db_file,)
    for row in cursor.execute("""
            SELECT date, username, segmented_body FROM tweets WHERE username LIKE ?
                OR username LIKE ? OR username LIKE ? OR username LIKE ?
            """, usernames
        ):
        name = row[1].lower()
        username_files[name].write("%s,%s\n" % (row[0], row[2],))

