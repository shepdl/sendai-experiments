import codecs
import glob

usernames = ["tranquil_dragon", "OfficialTEPCO", "kantei", "namicoaoto",]

for name in usernames:
    out_file = codecs.open("yoh_users/%s.csv" % (name,), "w", encoding="utf8")
    for filename in glob.glob("yoh_users/%s-from-*" % (name,)):
        print filename
        in_file = codecs.open(filename, encoding="utf8")
        in_file.readline()
        for line in in_file:
            out_file.write(line)
    out_file.close()
