#!/usr/bin/python
# coding=utf8
import os
import codecs
from utility import *
import sqlite3
import project

def dump_data(filename):
    print filename
# conn = sqlite3.connect(os.path.join(os.environ["SCRATCH"], "sendai-data", "20110316.db"))
    conn = sqlite3.connect(filename)
    cursor = conn.cursor()

    out = codecs.open(os.path.join(os.environ["SCRATCH"], 
            "sendai-segment-scratch", "%s-tweet-dump.txt" % (date_from(filename),)
        ), 
        "w",
        encoding="utf-8"
    )

    counter = 0
    inner_counter = 0
    for row in cursor.execute("SELECT id, body FROM tweets"):
        body = row[1]
        body = body.replace("\n", " ")
        body = body.replace("\\n", "")
        body = filter(
            lambda word: word[0:4] != "http" and word[1:5] != "http" and word[0] != "@", 
            body.split()
        )
        out.write("%s:%s\n" % (row[0], ' '.join(body),))
        counter += 1
        if counter % 100000 == 0:
            counter = 0
            inner_counter += 1
            print "%s00000 rows done" % (inner_counter,)

    cursor.close()
    out.close()

files = distribute(project.DATASET, dump_data)
